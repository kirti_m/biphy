package com.biphy.component;
import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.MouseInfo;
import java.awt.PointerInfo;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import javax.imageio.ImageIO;

import com.biphy.util.ResourceUtil;

public class GetMousePointer {
    public static void main(String[] args) {
        final String USER_HOME = System.getProperty("user.home");
    	URL recordIconImg = ResourceUtil.getImage("pointer.png");

        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        int width = gd.getDisplayMode().getWidth();
        int height = gd.getDisplayMode().getHeight();

        BufferedImage blackSquare = new BufferedImage(50, 50, BufferedImage.TYPE_3BYTE_BGR);
        for(int i = 0; i < blackSquare.getHeight(); i++){
            for(int j = 0; j < blackSquare.getWidth(); j++){
                blackSquare.setRGB(j, i, 128);
            }
        }

		String thumbPath = "biphy/src/main/resources/img/arrows-16x16.png";


        try {
            Robot robot = new Robot();
            BufferedImage screenshot = robot.createScreenCapture(new Rectangle(0,0,width,height));
            PointerInfo pointer = MouseInfo.getPointerInfo();
            int x = (int) pointer.getLocation().getX();
            int y = (int) pointer.getLocation().getY();

            screenshot.getGraphics().drawImage(ImageIO.read(recordIconImg), x, y, null);
            ImageIO.write(screenshot, "PNG", new File(USER_HOME, "screenshot.PNG"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}  

