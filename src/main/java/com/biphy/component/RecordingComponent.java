package com.biphy.component;

import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.controlsfx.control.Notifications;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;

import com.biphy.custom.widgets.AlertDialog;
import com.biphy.custom.widgets.CustomNotification;
import com.biphy.event.listener.RectangleListener;
import com.biphy.model.BiphyRecord;
import com.biphy.util.FileUtil;
import com.biphy.util.ResourceUtil;
import com.biphy.util.os.MacOsUtil;
import com.biphy.util.progress.ProgressFileBody;
import com.biphy.workplace.OperatingSystem;
import com.biphy.workplace.Workplace;
import com.madgag.gif.fmsware.AnimatedGifEncoder;

import javafx.application.Platform;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class RecordingComponent {

	public RectangleListener rectangleListener;

	private static final int fps = 4;

	private Timer screenCaptureTimer;
	private TimerTask screenCaptureTimerTask;
	private long imageIndex = 0;
	private String uuid;
	private FileOutputStream fileOutputStream;
	private AnimatedGifEncoder gifEncoder;
	private String filePath;
	private BufferedImage thumbnail;
	private String thumbnailPath;
	private File directory;
	private Bounds screenBounds;
	private BiphyRecord record;
	private int width;
	private int height;
	private CustomNotification customNotification;
	private URL mousePointer = (Workplace.isMacOs()?ResourceUtil.getImage("pointer.png"):ResourceUtil.getImage("mouse_cursor_win.png"));

	private Logger logger = Logger.getLogger(RecordingComponent.class);

	public void setRectangle(RectangleListener rectangle) {
		this.rectangleListener = rectangle;
	}

	public synchronized void startRecording() {
		uuid = new Date().toString();
		uuid = uuid.replace(" ", "").replace(":", "");
		Rectangle recordingRectangle = rectangleListener.getRectangle();
		screenBounds = recordingRectangle.getBoundsInLocal();
		int x = (int) recordingRectangle.getX();
		int y = (int) recordingRectangle.getY();
		width = (int) recordingRectangle.getWidth();
		height = (int) recordingRectangle.getHeight();
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				rectangleListener.startRecording(recordingRectangle);
				createOutputStream();
				java.awt.Rectangle screenRect = new java.awt.Rectangle(x, y, width, height);
				gifEncoder = new AnimatedGifEncoder();
				gifEncoder.setFrameRate(fps);
				gifEncoder.start(fileOutputStream);
				gifEncoder.setDelay(Math.round(1000f/fps));
				gifEncoder.setRepeat(0);
				Workplace.setRecording(true);
				try {
					Robot robot = new Robot();
					System.gc();
					screenCaptureTimerTask = new TimerTask() {

						@Override
						public void run() {
							Point pointer = MouseInfo.getPointerInfo().getLocation();
							try {
									addToGif(robot.createScreenCapture(screenRect), imageIndex,pointer,screenRect);

							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							imageIndex++;
						}
					};
					screenCaptureTimer = new Timer();
					screenCaptureTimer.scheduleAtFixedRate(screenCaptureTimerTask, 0L, 1000 / fps);
				} catch (AWTException e) {
					// TODO Auto-generated catch block

				}
			}
		});

	}

	private void addToGif(BufferedImage image, long imageIndex) {
		if (image != null && gifEncoder != null) {
			try {
				BufferedImage compressedImage = compressedImage(image);
				if ((imageIndex % 10 == 0) && image != null) {
					thumbnail = image;
				}
				gifEncoder.addFrame(compressedImage);
			} catch (Exception e) {
				logger.error("Add to gif error", e);
			}

		}
	}

	private void addToGif(BufferedImage image, long imageIndex,Point screenPoints , java.awt.Rectangle screenRect) throws IOException {
		synchronized (image) {

			if (image != null && gifEncoder != null) {
				if(screenRect.contains(screenPoints)){
					image.getGraphics().drawImage(ImageIO.read(mousePointer),screenPoints.x - screenRect.x,screenPoints.y - screenRect.y, null);
				}


				try {
					BufferedImage compressedImage = compressedImage(image);
					if ((imageIndex % 10 == 0) && image != null) {
						thumbnail = image;
					}

					gifEncoder.addFrame(compressedImage);

				} catch (Exception e) {
					logger.error("Add to gif error", e);
				}

			}
		}
	}

	private void createOutputStream() {
		try {
			directory = Workplace.getTempFolder();
			filePath = directory.getAbsolutePath() + Workplace.fileSeprator() + uuid + ".gif";
			fileOutputStream = new FileOutputStream(filePath);
		} catch (FileNotFoundException e) {
			logger.error("Create output stream error", e);
		}
	}

	private String uploadToServer(String filePath, String filename) throws ParseException, IOException {

		// Trust own CA and all self-signed certs
		SSLContext sslcontext;
		SSLConnectionSocketFactory sslsf;
		try {

			File certFile = null;

			if (Workplace.isProduction) {
				if (Workplace.isMacOs()) {
					certFile = ((MacOsUtil) Workplace.getOsUtil()).getMacResource("com.biphy.cert.file");
				} else {
					String certificatePath = ResourceUtil.getCertProd("cert/biphy-cert.keystore").getFile();
					certificatePath = certificatePath.replaceAll("%20", " ");
					certFile = new File(certificatePath);
				}

			} else {
				certFile = new File(ResourceUtil.getCertificate("biphy-cert.keystore").getFile());
			}

			sslcontext = SSLContexts.custom()
					.loadTrustMaterial(certFile, "changeit".toCharArray(), new TrustSelfSignedStrategy()).build();
			// Allow TLSv1 protocol only
			sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1" }, null,
					SSLConnectionSocketFactory.getDefaultHostnameVerifier());
		} catch (Exception e) {
			System.out.println("Exception occured while uploading to server");
			e.printStackTrace();
			logger.error("exception occured in uploading to server", e);
			return null;
		}

		CloseableHttpClient client = HttpClients.custom().setSSLSocketFactory(sslsf).build();
		ProgressFileBody fileBody = new ProgressFileBody(new File(filePath),
				ContentType.create("application/octet-stream"), filename);

		HttpEntity entity = MultipartEntityBuilder.create().addPart("upload_file", fileBody).build();

		HttpPost httpPost = new HttpPost("https://www.biphy.co/api/File/UploadByClient");
		httpPost.setEntity(entity);
		HttpResponse response = client.execute(httpPost);
		HttpEntity entity1 = response.getEntity();

		String content = EntityUtils.toString(entity1);
		return content;
	}

	private void createThumbnail() {
		if (thumbnail != null) {
			try {
				thumbnailPath = Workplace.getThumbnailPath().getAbsolutePath() + "" + Workplace.fileSeprator() + uuid
						+ "-thumb.jpg";

				FileOutputStream fileOutputStream = new FileOutputStream(thumbnailPath);
				ImageIO.write(thumbnail, "jpg", fileOutputStream);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				logger.error("createThumbnail file not found", e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error("createThumbnail ioexception", e);
			}
		}

	}

	private BufferedImage compressedImage(BufferedImage bufferedImage) {
		BufferedImage bimage = Scalr.resize(bufferedImage, Method.AUTOMATIC, Mode.FIT_EXACT, width, height);
		return bimage;
	}

	public void stopRecording() {
		screenCaptureTimer.cancel();
		try {
			gifEncoder.finish();
			fileOutputStream.close();
			createThumbnail();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("stoprecording ioexception", e);
		}

		if (Workplace.isMacOs()) {
			Workplace.getOsUtil().destroyMacRectangle();
		}
		Workplace.setRecording(false);
		// writer for biphyrecodrd.json
		customNotification = CustomNotification.getCustomNotification(uuid, "");

		try {
			customNotification.addGraphic(new Image(new File(thumbnailPath).toURI().toURL().toExternalForm()));
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		record = new BiphyRecord(uuid, filePath, FileUtil.getFileSize(filePath), filePath, null, thumbnailPath,
				FileUtil.getCreatedOn());

		BiphyRecord compressedRecord = null;
		try {
			compressedRecord = Workplace.getOsUtil().compressGif(Workplace.getWorkplace().getBiphyRecords(),
					new File(filePath), record);
		} catch (Exception e) {
			logger.error("Exception occured while compressing gif : ", e);
		}

		if (compressedRecord != null) {
			record = compressedRecord;
		}

		Thread uploadToServer = new Thread(new Runnable() {
			@Override
			public void run() {
				String remoteFilePath = null;
				try {
					remoteFilePath = uploadToServer(record.getLocalFilePath(), record.getFileId());
					System.out.println("Remote File Path : " + remoteFilePath);
					logger.info("remote File path : " + remoteFilePath);
					if (remoteFilePath == null) {
						return;
					}
					record.setRemoteFilePath(remoteFilePath);

					Workplace.getWorkplace().getBiphyRecords().addRecordWithoutPopup(record);
					Platform.runLater(() -> {
						customNotification.changeText("Your bif is ready !");
						customNotification.addDeleteOption(record);
						customNotification = customNotification.build();
					});

				} catch (ParseException | IOException e) {
					logger.error("Error occured while uploading to server", e);
				}
			}
		});
		uploadToServer.start();

	}

	public void cancelRecording() {
		if (screenCaptureTimer != null && gifEncoder != null && fileOutputStream != null) {
			screenCaptureTimer.cancel();
			try {
				gifEncoder.finish();
				fileOutputStream.close();
				Files.deleteIfExists(new File(filePath).toPath());
				if (Workplace.getOs().equals(OperatingSystem.MACOS)) {
					Workplace.getOsUtil().destroyMacRectangle();
				}
				Workplace.setRecording(false);
			} catch (IOException e) {
				logger.error("stoprecording ioexception", e);
			}
		}
	}

}
