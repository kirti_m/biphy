package com.biphy.component;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.biphy.event.listener.TrayMenuPopupListener;
import com.biphy.model.BiphyRecord;
import com.biphy.util.BiphyRecordReader;
import com.biphy.util.BiphyRecordWriter;
import com.biphy.workplace.Workplace;

import javafx.application.Platform;

public class BiphyRecords {

	private List<BiphyRecord> biphyRecordList;
	private BiphyRecordReader biphyRecordReader;
	private BiphyRecordWriter biphyRecordWriter;
	private TrayMenuPopupListener trayMenuPopup;

	public BiphyRecords(File userFile) {
		super();
		this.biphyRecordReader = new BiphyRecordReader(userFile);
		this.biphyRecordWriter = new BiphyRecordWriter(userFile);
		this.biphyRecordList = biphyRecordReader.readBiphyRecord();
		if (biphyRecordList == null) {
			biphyRecordList = new ArrayList<BiphyRecord>();
		}
		Collections.sort(biphyRecordList);

	}

	public void addBiphyRecord(BiphyRecord record) {
		if (record == null) {
			return;
		}
		trayMenuPopup = Workplace.getCustomSystemTrayPopup();
		this.biphyRecordList.add(record);
		biphyRecordWriter.writeBiphyRecord(biphyRecordList);
		if (Platform.isFxApplicationThread()) {
			trayMenuPopup.addToPopup(record);
		} else {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					trayMenuPopup.addToPopup(record);
				}
			});
		}
	}

	public void removeBiphyRecord(BiphyRecord record) {
		trayMenuPopup = Workplace.getCustomSystemTrayPopup();
		biphyRecordList.remove(record);
		biphyRecordWriter.writeBiphyRecord(biphyRecordList);
		if (Platform.isFxApplicationThread()) {
			trayMenuPopup.removeRecord(record);
		} else {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					trayMenuPopup.removeRecord(record);
				}
			});
		}

	}

	public synchronized void replaceRecord(BiphyRecord record) {
		if (record == null) {
			return;
		}
		BiphyRecord removeRecord = null;
		for (BiphyRecord biphyRecord : biphyRecordList) {
				if (biphyRecord.getFileId().equals(record.getFileId())) {
					removeRecord = biphyRecord;
			}
		}
		biphyRecordList.remove(removeRecord);
		biphyRecordList.add(record);
		biphyRecordWriter.writeBiphyRecord(biphyRecordList);
	}

	public synchronized void addRecordWithoutPopup(BiphyRecord record){
		this.biphyRecordList.add(record);
		Collections.sort(biphyRecordList);
		biphyRecordWriter.writeBiphyRecord(biphyRecordList);
	}

	public List<BiphyRecord> getBiphyRecords() {
		return biphyRecordList;
	}

}
