package com.biphy.workplace;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.biphy.component.BiphyRecords;
import com.biphy.custom.widgets.AlertDialog;
import com.biphy.custom.widgets.CustomRectangle;
import com.biphy.custom.widgets.CustomSystemTrayIcons;
import com.biphy.custom.widgets.CustomTrayPopup;
import com.biphy.main.RecorderScene;
import com.biphy.ui.service.MapRecorderScreenService;
import com.biphy.ui.service.ShowRecordService;
import com.biphy.ui.service.SplashScreenService;
import com.biphy.ui.service.TrayIconsService;
import com.biphy.ui.service.TrayMenuPopupService;
import com.biphy.util.ResourceUtil;
import com.biphy.util.ScreenUtil;
import com.biphy.util.os.MacOsUtil;
import com.biphy.util.os.OsUtil;
import com.biphy.util.os.WindowOsUtil;

import javafx.scene.Scene;
import javafx.stage.Stage;

public class Workplace {

	/*
	 * Services will be initialized in begining
	 */
	private static Map<BiphyServices, RecorderScene> recordingService = new TreeMap<>();

	private static CustomRectangle customRecordingRectangle;

	private static CustomSystemTrayIcons customSystemTrayIcons;

	private static CustomTrayPopup customSystemTrayPopup;

	private static Scene scene;

	private static Stage stage;

	private static File tempFolder;

	public static OperatingSystem os;

	private static Workplace workplace;

	private static File userData;

	private static File thumbnailPath;

	private BiphyRecords biphyRecords;

	private static OsUtil osUtil;

	private static Logger logger;

	public static boolean isProduction;

	private static boolean isRecording;

	private static ScreenUtil screenUtil;

	private static File loggerFile;

	private static final String appPackageName = "com.biphy.main.BiphyRecorder";
	public static final String appName = "BiphyRecorder";

	public Workplace() {
		super();
		biphyRecords = new BiphyRecords(userData);
	}

	public static void initialiseService(Stage stage, boolean production) {
		isProduction = production;
		setStage(stage);
		getOS();
		createLoggerFile();
		recordingService.put(BiphyServices.Map_Recorder_Screen, new MapRecorderScreenService(stage));
		recordingService.put(BiphyServices.Show_Record, new ShowRecordService(stage));
		recordingService.put(BiphyServices.Tray_Icon, new TrayIconsService(stage));
		recordingService.put(BiphyServices.Tray_Menu_Popup, new TrayMenuPopupService(stage, null));
		recordingService.put(BiphyServices.Splash_Screen, new SplashScreenService(stage));
		logger = Logger.getLogger(Workplace.class);
		createTempFolder();
		initialiseWorkSpace();
		
		if (isProduction) {
			setResourceUtil();
		}
		screenUtil = new ScreenUtil();
	}

	private static String getResourceFilePath() {
		File file = new File(".");
		String absolutePath = null;
		String folderPath = null;
		if (isMacOs()) {
			absolutePath = FilenameUtils.getFullPathNoEndSeparator(file.getAbsolutePath());
			folderPath = FilenameUtils.getFullPathNoEndSeparator(absolutePath);
		} else {
			absolutePath = file.getAbsolutePath();
			folderPath = absolutePath.substring(0, file.getAbsolutePath().length() - 1);
		}
		return folderPath;
	}

	private static void setResourceUtil() {
		String folderPath = getResourceFilePath();
		ResourceUtil.setSetupPath(folderPath + Workplace.fileSeprator() + "lib" + Workplace.fileSeprator());
	}

	private static void initialiseWorkSpace() {
		workplace = new Workplace();
	}

	public static RecorderScene getService(BiphyServices serviceName) {
		if (recordingService.containsKey(serviceName)) {
			return recordingService.get(serviceName);
		}
		return null;
	}

	public static void updateService(BiphyServices serviceName, RecorderScene service) {
		if (recordingService.containsKey(serviceName)) {
			recordingService.put(serviceName, service);
		}
	}

	public static CustomRectangle getCustomRecordingRectangle() {
		return customRecordingRectangle;
	}

	public static void setCustomRecordingRectangle(CustomRectangle customRecordingRectangle) {
		Workplace.customRecordingRectangle = customRecordingRectangle;
	}

	public static CustomSystemTrayIcons getCustomSystemTrayIcons() {
		return customSystemTrayIcons;
	}

	public static void setCustomSystemTrayIcons(CustomSystemTrayIcons customSystemTrayIcons) {
		Workplace.customSystemTrayIcons = customSystemTrayIcons;
	}

	public static CustomTrayPopup getCustomSystemTrayPopup() {
		return customSystemTrayPopup;
	}

	public static void setCustomSystemTrayPopup(CustomTrayPopup customSystemTrayPopup) {
		Workplace.customSystemTrayPopup = customSystemTrayPopup;
	}

	public static Scene getScene() {
		return scene;
	}

	public static void setScene(Scene scene) {
		Workplace.scene = scene;
	}

	public static Stage getStage() {
		return stage;
	}

	public static void setStage(Stage stage) {
		Workplace.stage = stage;
	}

	public BiphyRecords getBiphyRecords() {
		return biphyRecords;
	}

	public static File getThumbnailPath() {
		return thumbnailPath;
	}

	public static OperatingSystem getOs() {
		return os;
	}

	public static File getTempFolder() {
		return tempFolder;
	}

	public static Workplace getWorkplace() {
		if (workplace == null) {
			workplace = new Workplace();
		}
		return workplace;
	}

	public static File getUserData() {
		return userData;
	}

	public static OsUtil getOsUtil() {
		return osUtil;
	}

	private static void createTempFolder() {
		final String tempFolderName = "/BiphyRecorder";
		try {
			String tempPath = System.getProperty("user.home");
			tempFolder = new File(tempPath + tempFolderName);
			if (!tempFolder.exists()) {
				tempFolder.mkdir();
			}

			if (tempFolder.exists()) {
				createThumbnailFolder();
				createUserDataFile();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void createThumbnailFolder() {
		File thumbnail = new File(tempFolder.getAbsolutePath() + "" + fileSeprator() + "thumbnail");
		if (!thumbnail.exists()) {
			thumbnail.mkdir();
		}
		thumbnailPath = thumbnail;
	}

	public static String fileSeprator() {
		return (os.equals(OperatingSystem.Window) ? "\\" : "/");
	}

	private static String split(String pathName) {
		String str = (os.equals(OperatingSystem.Window) ? "\\" : "/");
		String path = pathName.substring(0, pathName.lastIndexOf(str));
		return path.concat(str);
	}

	private static void getOS() {
		String OS = System.getProperty("os.name").toLowerCase();
		if (OS.indexOf("win") >= 0) {
			os = OperatingSystem.Window;
			osUtil = new WindowOsUtil();
		} else if (OS.indexOf("mac") >= 0) {
			os = OperatingSystem.MACOS;
			if (isProduction) {
				Map<String, File> librariesFilesPath = getMacOsConfigFiles();
				File projectFile  = null;
				if(!librariesFilesPath.containsKey("com.biphy.app.path")) {
					projectFile = MacOsUtil.getAppFile(MacOsUtil.appName);
					ResourceUtil.addToPropertyFile("com.biphy.app.path", projectFile.getAbsolutePath());	
				}else {
					projectFile = librariesFilesPath.get("com.biphy.app.path");
				}
				File projectPath = new File(FilenameUtils.getFullPathNoEndSeparator(projectFile.getAbsolutePath()));
				createLoggerFile(projectPath);
				osUtil = new MacOsUtil(librariesFilesPath);
			} else {
				osUtil = new MacOsUtil();
			}
		}
	}

	private static Map<String, File> getMacOsConfigFiles() {
		Map<String, File> librariesFilesPath = new HashMap<>();
		try {
			File macOsConfigPropertiesFile = ResourceUtil.getMacConfigFile();
			System.out.println("Mac os conf file : " + macOsConfigPropertiesFile.getAbsolutePath());
			if (macOsConfigPropertiesFile != null) {
				InputStream macOsConfigPropertiesStream = new FileInputStream(macOsConfigPropertiesFile);
				Properties properties = new Properties();
				properties.load(macOsConfigPropertiesStream);
				for (Object keysObject : properties.keySet()) {
					String key = (String) keysObject;
					if (key != null) {
						File macOsFile = new File(ResourceUtil.getMacOsFiles(properties.getProperty(key)));
						if (macOsFile.exists()) {
							librariesFilesPath.put(key, macOsFile);
						}
					}

				}
			}
		} catch (Exception e) {
			if (logger != null) {
				logger.error("Error occured while reading macos config properties ", e);
			}
			e.printStackTrace();

		}

		return librariesFilesPath;

	}

	private static boolean setPermission(File file) {
		Set<PosixFilePermission> perms = new HashSet<>();
		perms.add(PosixFilePermission.OWNER_READ);
		perms.add(PosixFilePermission.OWNER_WRITE);
		perms.add(PosixFilePermission.OWNER_EXECUTE);

		perms.add(PosixFilePermission.OTHERS_READ);
		perms.add(PosixFilePermission.OTHERS_WRITE);
		perms.add(PosixFilePermission.OTHERS_EXECUTE);

		perms.add(PosixFilePermission.GROUP_READ);
		perms.add(PosixFilePermission.GROUP_WRITE);
		perms.add(PosixFilePermission.GROUP_EXECUTE);

		try {
			Set<PosixFilePermission> presentPermission = Files.getPosixFilePermissions(file.toPath(),
					LinkOption.NOFOLLOW_LINKS);
			if (presentPermission.containsAll(perms)) {
				return true;
			}

			Files.setPosixFilePermissions(file.toPath(), perms);
			return true;
		} catch (Exception e) {
			if (logger != null) {
				logger.error("Error occured while giving permission to : " + file.getAbsolutePath(), e);
			} else {
				System.out.println("Error occured while giving permission to : " + file.getAbsolutePath());
				e.printStackTrace();
			}
			return false;
		}

	}

	public static boolean checkIfApplicationIsRunning() {
		if (isApplicationRunning() == true) {
			AlertDialog alertDialog = new AlertDialog(stage, "Application Running",
					"Another instance of application is already running");
			alertDialog.showWidgetAndCloseApplication();
			return true;
		} else {
			return false;
		}
	}

	private static boolean isApplicationRunning() {
		return osUtil.checkIfApplicationIsRunning();
	}

	private static void createUserDataFile() {
		File bin = new File(tempFolder.getAbsolutePath() + "/bin");
		try {
			boolean fileCreated = false;
			if (!bin.exists()) {
				bin.mkdir();
			}
			File userDataFile = new File(bin.getAbsolutePath() + "/user_info.json");
			if (!userDataFile.exists()) {
				fileCreated = userDataFile.createNewFile();
			}

			if (userDataFile.exists()) {
				userData = userDataFile;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private static void createLoggerFile() {
		String projectPath = getResourceFilePath();
		if (loggerFile == null) {
			File logsFolder = new File(projectPath + "/logs");
			if (!logsFolder.exists()) {
				logsFolder.mkdir();
			}
			loggerFile = logsFolder;
			File loggerFile = new File(
					projectPath + "/logs/" + new Date().toString().replace(" ", "_").replace(":", "_") + ".log");
			if (!loggerFile.exists()) {
				try {
					loggerFile.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			Properties properties = new Properties();
			try {
				InputStream inputStream = ResourceUtil.getConfig("log4j.properties").openStream();
				properties.load(inputStream);
				properties.put("log4j.appender.FILE.File", loggerFile.getAbsolutePath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PropertyConfigurator.configure(properties);
		}

	}

	private static void createLoggerFile(File projectPath) {
		if (loggerFile == null) {
			File logsFolder = new File(projectPath + "/logs");
			if (!logsFolder.exists()) {
				logsFolder.mkdir();
			}
			loggerFile = logsFolder;
			File loggerFile = new File(
					projectPath + "/logs/" + new Date().toString().replace(" ", "_").replace(":", "_") + ".log");
			if (!loggerFile.exists()) {
				try {
					loggerFile.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			Properties properties = new Properties();
			try {
				InputStream inputStream = ResourceUtil.getConfig("log4j.properties").openStream();
				properties.load(inputStream);
				properties.put("log4j.appender.FILE.File", loggerFile.getAbsolutePath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PropertyConfigurator.configure(properties);
		}

	}

	public static boolean isWindows() {
		if (Workplace.getOs().equals(OperatingSystem.Window)) {
			return true;
		}
		return false;
	}

	public static boolean isMacOs() {
		if (Workplace.getOs().equals(OperatingSystem.MACOS)) {
			return true;
		}
		return false;
	}

	public static boolean isRecording() {
		return isRecording;
	}

	public static void setRecording(boolean isRecording) {
		Workplace.isRecording = isRecording;
	}

	public static ScreenUtil getScreenUtil() {
		return screenUtil;
	}

}
