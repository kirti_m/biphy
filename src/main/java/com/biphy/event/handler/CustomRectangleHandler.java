package com.biphy.event.handler;

import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;

import com.biphy.event.listener.RectangleListener;
import com.biphy.util.ResourceUtil;
import com.biphy.workplace.OperatingSystem;
import com.biphy.workplace.Workplace;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.stage.Screen;

public class CustomRectangleHandler {

	private Point2D dragStarted;

	private Point2D dragEnded;

	private RectangleListener rectangleListnener;

	private Cursor cursor = Cursor.cursor((Workplace.isWindows()?ResourceUtil.getImage("arrows.png").toExternalForm():ResourceUtil.getImage("arrows-32x32.png").toExternalForm()));


	public CustomRectangleHandler(RectangleListener rectangleListnener) {
		super();
		this.rectangleListnener = rectangleListnener;
	}

	public class CustomRectangleMousePressedHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			dragStarted = new Point2D(event.getSceneX(), event.getSceneY());
		}

	}

	public class CustomRectangleMouseDraggedHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			Platform.runLater(new Runnable() {

				@Override
				public void run() {
					rectangleListnener.updateCursor(cursor);
					dragEnded = new Point2D(event.getSceneX(), event.getSceneY());
					rectangleListnener.relocalteRectangle(dragStarted, dragEnded);
				}
			});

		}

	}

	public class CustomRectangleMouseReleasedHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			dragEnded = new Point2D(event.getSceneX(), event.getSceneY());
			rectangleListnener.updateCursor(Cursor.DEFAULT);
			rectangleListnener.notifyMove(dragStarted, dragEnded);
		}

	}

	public class CustomRectangleHoverHandler implements EventHandler<MouseEvent> {

		private boolean entered;


		public CustomRectangleHoverHandler(boolean entered) {
			super();
			this.entered = entered;
		}

		@Override
		public void handle(MouseEvent event) {
			if (entered) {
				rectangleListnener.updateCursor(cursor);
			} else {
				rectangleListnener.updateCursor(Cursor.DEFAULT);
			}
		}

	}

	public class CustomRectangleNativeEventHandler implements NativeMouseInputListener, NativeKeyListener {

		private boolean mousePressed = false;
		private boolean mouseReleased = false;
		private boolean keyPressed = false;
		private boolean keyReleased = false;

		@Override
		public void nativeMouseClicked(NativeMouseEvent arg0) {
			if (Workplace.isWindows()) {
				rectangleListnener.windowsMakeStageActive(false);
			} else {
				rectangleListnener.windowsMakeStageActive(false);
			}
		}

		@Override
		public void nativeMousePressed(NativeMouseEvent arg0) {
			if (Workplace.isWindows()) {
				rectangleListnener.windowsMakeStageActive(false);
			} else {
				rectangleListnener.windowsMakeStageActive(false);
			}
		}

		@Override
		public void nativeMouseReleased(NativeMouseEvent arg0) {
			if (Workplace.isWindows()) {
				rectangleListnener.windowsMakeStageActive(false);
			} else {
				rectangleListnener.windowsMakeStageActive(false);
			}
		}

		@Override
		public void nativeMouseDragged(NativeMouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void nativeMouseMoved(NativeMouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void nativeKeyPressed(NativeKeyEvent arg0) {
			if (Workplace.getOs().equals(OperatingSystem.Window)) {
				rectangleListnener.windowsMakeStageActive(false);
			} else {
				rectangleListnener.windowsMakeStageActive(false);
			}
		}

		@Override
		public void nativeKeyReleased(NativeKeyEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void nativeKeyTyped(NativeKeyEvent arg0) {
			// TODO Auto-generated method stub

		}

	}

}
