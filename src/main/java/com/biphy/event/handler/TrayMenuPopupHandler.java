package com.biphy.event.handler;

import org.apache.log4j.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;

import com.biphy.event.listener.TrayMenuPopupListener;

import javafx.application.Platform;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class TrayMenuPopupHandler implements NativeMouseListener {

	private Stage stage;

	private TrayMenuPopupListener listener;

	private static Logger logger = Logger.getLogger(TrayMenuPopupHandler.class);

	public TrayMenuPopupHandler(Stage stage, TrayMenuPopupListener listener) {
		super();
		this.stage = stage;
		this.listener = listener;
	}

	@Override
	public void nativeMouseClicked(NativeMouseEvent mouseEvent) {
		removePopup(mouseEvent);
	}

	@Override
	public void nativeMousePressed(NativeMouseEvent mouseEvent) {
		removePopup(mouseEvent);
	}

	@Override
	public void nativeMouseReleased(NativeMouseEvent mouseEvent) {
		removePopup(mouseEvent);
	}

	private void removePopup(NativeMouseEvent mouseEvent) {
		System.out.println("remove popup called");
		Rectangle rectangle = new Rectangle(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight());
		logger.info("rectangle : " + rectangle);
		if (!rectangle.contains(mouseEvent.getX(), mouseEvent.getY())) {
			TrayMenuPopupHandler trayMenuPopupHandler = this;
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					listener.removePopup();
					logger.info("logger remove popup method called : ");
					GlobalScreen.removeNativeMouseListener(trayMenuPopupHandler);
					logger.info("logger remove native mouse handler called ");
				}
			});

		}
	}

}
