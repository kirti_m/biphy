package com.biphy.event.handler;

import java.util.HashMap;
import java.util.Map;

import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;

import com.biphy.custom.widgets.CustomRectangle.RectPoints;
import com.biphy.event.listener.EdgesCircleListnener;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;

public class RectangleEdgesHandler {

	private Point2D mouseClicked;

	private Point2D mouseReleased;

	private static Map<RectPoints, EdgesCircleListnener> edgesCircleListenerMap = new HashMap<>();

	private RectPoints rectPoints;

	private static RectPoints activeRectPoint = null;

	public RectangleEdgesHandler(EdgesCircleListnener edgesCircleListener, RectPoints rectPoints) {
		super();
		edgesCircleListenerMap.put(rectPoints, edgesCircleListener);
		this.rectPoints = rectPoints;
	}

	public class RectangleEdgesMousePressedHandler implements EventHandler<MouseEvent> {

		public void handle(MouseEvent event) {
			activeRectPoint = rectPoints;
			mouseClicked = new Point2D(event.getSceneX(), event.getSceneY());
			edgesCircleListenerMap.get(activeRectPoint).addToSceneHandler();
			edgesCircleListenerMap.get(activeRectPoint).addNativeHandler();
		}

	}

	public class RectangleEdgesMouseDraggedHandler implements EventHandler<MouseEvent> {

		public void handle(MouseEvent event) {
			if (activeRectPoint != null) {
				edgesCircleListenerMap.get(activeRectPoint).updateCursor(Cursor.DISAPPEAR, true);
				mouseReleased = new Point2D(event.getSceneX(), event.getSceneY());
				edgesCircleListenerMap.get(activeRectPoint).notifyResize(activeRectPoint, mouseReleased,true);
			}
		}

	}

	public class RectangleEdgesMouseReleasedHandler implements EventHandler<MouseEvent> {

		public void handle(MouseEvent event) {
			if (activeRectPoint != null) {
				edgesCircleListenerMap.get(activeRectPoint).updateCursor(Cursor.DEFAULT, false);
				mouseReleased = new Point2D(event.getSceneX(), event.getSceneY());
				edgesCircleListenerMap.get(activeRectPoint).notifyResize(rectPoints, mouseReleased,false);
				edgesCircleListenerMap.get(activeRectPoint).removeFromSceneHandler();
				edgesCircleListenerMap.get(activeRectPoint).removeNativeHandler();
				activeRectPoint = null;
			}

		}

	}

	public class RectangleEdgesNativeMouseHandler implements NativeMouseInputListener {

		@Override
		public void nativeMouseClicked(NativeMouseEvent arg0) {
		}

		@Override
		public void nativeMousePressed(NativeMouseEvent arg0) {
		}

		@Override
		public void nativeMouseReleased(NativeMouseEvent arg0) {
		}

		@Override
		public void nativeMouseDragged(NativeMouseEvent arg0) {
			if (activeRectPoint != null) {
				edgesCircleListenerMap.get(activeRectPoint).updateCursor(Cursor.DISAPPEAR, true);
				mouseReleased = new Point2D(arg0.getX(), arg0.getY());
				edgesCircleListenerMap.get(activeRectPoint).notifyResize(activeRectPoint, mouseReleased,true);
			}
		}

		@Override
		public void nativeMouseMoved(NativeMouseEvent arg0) {

		}

	}

}
