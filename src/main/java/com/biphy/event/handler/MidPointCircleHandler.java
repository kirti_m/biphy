package com.biphy.event.handler;

import com.biphy.event.listener.MidPointCircleListener;
import com.biphy.event.listener.RectangleListener;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;

public class MidPointCircleHandler {


	private Point2D mousePressed;

	private Point2D mouseReleased;

	private RectangleListener rectangleListener;

	private MidPointCircleListener circle;





	public MidPointCircleHandler(RectangleListener rectangleListener) {
		super();
		this.rectangleListener = rectangleListener;
	}

	public void setMidPointCircle(MidPointCircleListener circle){
		this.circle = circle;
	}

	public class MidPointCircleMousePressedHandler implements EventHandler<MouseEvent> {

		public void handle(MouseEvent event) {
			circle.addToSceneListener();
			mousePressed = new Point2D(event.getSceneX(),event.getSceneY());
		}

	}

	public class MidPointCircleMouseDraggedHandler implements EventHandler<MouseEvent> {

		public void handle(MouseEvent event) {
			circle.updateCursor(Cursor.MOVE);
			mouseReleased = new Point2D(event.getSceneX(),event.getSceneY());
			circle.midCircleDragged(mousePressed,mouseReleased,rectangleListener);
		}

	}

	public class MidPointCircleMouseReleasedHandler implements EventHandler<MouseEvent>{

		public void handle(MouseEvent event) {
			circle.updateCursor(Cursor.DEFAULT);
			mouseReleased = new Point2D(event.getSceneX(),event.getSceneY());
			circle.removeSceneListener();
			circle.midCircleDragged(mousePressed,mouseReleased,rectangleListener);
			rectangleListener.notifyMove(mousePressed, mouseReleased);

		}
	}

}
