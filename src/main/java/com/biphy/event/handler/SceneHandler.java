package com.biphy.event.handler;

import com.biphy.custom.widgets.CustomScene;
import com.biphy.event.listener.SceneListener;
import com.biphy.ui.service.ShowRecordService;
import com.biphy.util.RectangleUtil;
import com.biphy.util.ResourceUtil;
import com.biphy.workplace.Workplace;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class SceneHandler {

	private Point2D mouseClicked;

	private Point2D mouseReleased;

	private SceneListener sceneListener;

	private Rectangle rectangle;

	private Cursor cursor = Cursor.cursor(ResourceUtil.getImage("arrows.png").toExternalForm());


	public SceneHandler(SceneListener sceneListener) {
		super();
		this.sceneListener = sceneListener;
	}

	public Point2D getMouseClicked() {
		return mouseClicked;
	}

	public void setMouseClicked(Point2D mouseClicked) {
		this.mouseClicked = mouseClicked;
	}

	public Point2D getMouseReleased() {
		return mouseReleased;
	}

	public void setMouseReleased(Point2D mouseReleased) {
		this.mouseReleased = mouseReleased;
	}

	public class SceneMousePressedHandler implements EventHandler<MouseEvent> {

		public void handle(MouseEvent event) {
			mouseClicked = new Point2D(event.getSceneX(), event.getSceneY());

		}

	}

	public class SceneMouseDraggedHandler implements EventHandler<MouseEvent> {

		public void handle(MouseEvent event) {
			mouseReleased = new Point2D(event.getSceneX(), event.getSceneY());
			rectangle = RectangleUtil.getRect(mouseClicked, mouseReleased);
			sceneListener.updateCursor((Workplace.isMacOs()?Cursor.CROSSHAIR:cursor));
			sceneListener.updateScene(rectangle);

		}

	}

	public class SceneMouseReleasedHandler implements EventHandler<MouseEvent> {

		private ShowRecordService showRecordService;

		private Stage stage;

		private CustomScene customScene;

		public SceneMouseReleasedHandler(ShowRecordService showRecordService, Stage stage, CustomScene customScene) {
			this.showRecordService = showRecordService;
			this.stage = stage;
			this.customScene = customScene;
		}

		public void handle(MouseEvent event) {
			rectangle = RectangleUtil.getRect(mouseClicked, mouseReleased);
			sceneListener.updateCursor(Cursor.DEFAULT);
			sceneListener.removeAllHandler();
			sceneListener.addRectangle(rectangle);
			showRecordService.nextScene(stage, customScene);
		}

	}

}
