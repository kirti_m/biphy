package com.biphy.event.listener;

import javafx.scene.Cursor;
import javafx.scene.shape.Rectangle;

public interface SceneListener {

	public void updateScene(Rectangle rect);

	public void removeAllHandler();

	public void addRectangle(Rectangle rect);

	public void updateCursor(Cursor cursor);



}
