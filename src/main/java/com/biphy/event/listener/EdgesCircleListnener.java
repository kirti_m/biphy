package com.biphy.event.listener;

import com.biphy.custom.widgets.CustomRectangle.RectPoints;

import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;

public interface EdgesCircleListnener {

	public void notifyResize(RectPoints rectPoint, Point2D corordinate , boolean isDragged);

	public void addToSceneHandler();

	public void removeFromSceneHandler();

	public void updateCursor(Cursor cursor , boolean update);

	public void addNativeHandler();

	public void removeNativeHandler();

	public Shape getCircle();

	public void removeEventHandlers();

	public void relocateCircle(double x , double y);

	public void removeCircle();
}
