package com.biphy.event.listener;

import com.biphy.custom.widgets.CustomRectangle.RectPoints;
import com.biphy.custom.widgets.EdgesCircle;

import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.shape.Rectangle;

public interface RectangleListener {

	public void notifyResize(RectPoints direction, Point2D previousLocation, Point2D newLocation);

	public void notifyMove(Point2D previousLocation, Point2D newLocation);

	// public void updateMidPointCircle(MidPointCircle circle);

	public void updateEdgesCorordinate(RectPoints direction, EdgesCircle edgeCircle);

	public void updateCursor(Cursor cursor);

	public Rectangle getRectangle();

	public void startRecording(Rectangle rectangle);

	public void removeRectangle();

	public void addNativeHandler();

	public void removeNativeHandler();

	public void windowsMakeStageActive(boolean makeFullScreen);

	public void relocalteRectangle(Point2D previousLocation, Point2D newLocation);

	public void notifyDrag(RectPoints direction, Point2D previousLocation, Point2D newLocation);
}
