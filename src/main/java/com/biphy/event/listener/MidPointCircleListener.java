package com.biphy.event.listener;

import javafx.geometry.Point2D;
import javafx.scene.Cursor;

public interface MidPointCircleListener {

	public void midCircleDragged(Point2D oldPosition , Point2D newPosition , RectangleListener rectangleListener);

	public void addToSceneListener();

	public void removeSceneListener();

	public void updateCursor(Cursor cursor);

}
