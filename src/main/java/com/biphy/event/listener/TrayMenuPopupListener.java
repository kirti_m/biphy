package com.biphy.event.listener;

import com.biphy.model.BiphyRecord;

import javafx.geometry.Point2D;

public interface TrayMenuPopupListener {

	public void removePopup();

	public void showPopup(Point2D point);

	public void addToPopup(BiphyRecord biphyRecord);

	public void removeRecord(BiphyRecord biphyRecord);
}
