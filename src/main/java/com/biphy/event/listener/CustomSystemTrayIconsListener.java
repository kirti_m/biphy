package com.biphy.event.listener;

import com.biphy.component.RecordingComponent;

public interface CustomSystemTrayIconsListener {

	public void addRecordButton(RectangleListener rectangle);

	public void removeRecordButton();

	public void addStopButton();

	public void addStopButton(RecordingComponent recordingComponent);

	public void removeStopButton();

	public void addApplicationIcon();

	public void removeApplicationIcon();

	public void addCrossHairIcon();

	public void removeCrossHairIcon();


}
