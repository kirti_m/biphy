package com.biphy.custom.widgets;

import java.awt.AWTEvent;
import java.awt.AWTException;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.MenuItem;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URL;
import java.util.logging.LogManager;

import javax.imageio.ImageIO;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.apache.log4j.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;

import com.biphy.component.RecordingComponent;
import com.biphy.event.listener.CustomSystemTrayIconsListener;
import com.biphy.event.listener.RectangleListener;
import com.biphy.ui.service.TrayMenuPopupService;
import com.biphy.util.JavaFxDispatchService;
import com.biphy.util.ResourceUtil;
import com.biphy.workplace.BiphyServices;
import com.biphy.workplace.OperatingSystem;
import com.biphy.workplace.Workplace;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class CustomSystemTrayIcons extends CustomWidget implements CustomSystemTrayIconsListener {

	public CustomSystemTrayIcons(Scene scene, Stage stage) {
		super(scene, stage);
		recordingComponent = new RecordingComponent();
		if (Workplace.isMacOs()) {
			createMacExitMenu();
		}
	}

	private Rectangle recordingRectangle;
	
	static boolean isTrayOpen=false;

	private SystemTray tray = SystemTray.getSystemTray();

	private static final URL recordIconImg = ResourceUtil.getImage("record-system-tray.png");

	private static final URL stopButtonImg = ResourceUtil.getImage("stop-system-tray-icon-16x16.png");

	private static final URL applicationImg = ResourceUtil.getImage("biphy-tray-icon-purple-16x16.png");

	private static final URL crossHairImg = ResourceUtil.getImage("cross-hair-icon-16x16.png");

	private TrayIcon recordIcon;

	private TrayIcon stopIcon;

	private TrayIcon applicationIcon;

	private TrayIcon crossHairIcon;

	private RecordingComponent recordingComponent;

	private Logger logger = Logger.getLogger(CustomSystemTrayIcons.class);

	private SystemTrayNativeMouseHandler systemTrayNativeMouseHandler;

	private JPopupMenu jPopupMenu;

	@Override
	public void notifyListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addRecordButton(RectangleListener rectangle) {
		removeAllIcon();
		java.awt.Image image;
		try {
			image = ImageIO.read(recordIconImg);
			recordIcon = new TrayIcon(image);
			recordIcon.setToolTip(Workplace.appName);
			recordIcon.addMouseListener(new MouseListener() {

				@Override
				public void mouseReleased(MouseEvent arg0) {

				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					if (arg0.getButton() == MouseEvent.BUTTON1 && !arg0.isPopupTrigger()) {
						removeRecordButton();
						addStopButton();
						Platform.runLater(new Runnable() {

							@Override
							public void run() {
								recordingRectangle = rectangle.getRectangle();
								recordingComponent.setRectangle(rectangle);
								recordingComponent.startRecording();
							}
						});
					}
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseClicked(MouseEvent arg0) {
					// TODO Auto-generated method stub

				}
			});
			addToSystemTray(recordIcon);
		} catch (IOException | AWTException e) {
			logger.error("add record button", e);
		}
		Workplace.getCustomSystemTrayIcons().setScene(rectangle.getRectangle().getScene());
	}

	@Override
	public void removeRecordButton() {
		if (recordIcon != null) {
			tray.remove(recordIcon);
		}
	}

	@Override
	public void addStopButton() {
		if (stopIcon != null) {
			if (contains(stopIcon)) {
				return;
			}
		}
		removeAllIcon();
		java.awt.Image image;
		try {
			image = ImageIO.read(stopButtonImg);
			stopIcon = new TrayIcon(image);
			stopIcon.setToolTip(Workplace.appName);
			stopIcon.addMouseListener(new MouseListener() {

				@Override
				public void mouseReleased(MouseEvent arg0) {

				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					if (arg0.getButton() == MouseEvent.BUTTON1 && !arg0.isPopupTrigger()) {
						removeStopButton();
						if (Platform.isFxApplicationThread()) {
							Workplace.getStage().close();
						} else {
							Platform.runLater(new Runnable() {
								@Override
								public void run() {
									Workplace.getStage().close();
								}
							});
						}

						recordingComponent.stopRecording();

					}
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseClicked(MouseEvent arg0) {
					// TODO Auto-generated method stub

				}
			});
			addToSystemTray(stopIcon);
		} catch (IOException | AWTException e) {
			// TODO Auto-generated catch block
			logger.error("add stop button", e);
		}
	}

	private boolean contains(TrayIcon trayIcon) {
		for (TrayIcon trayIcon2 : tray.getTrayIcons()) {
			if (trayIcon2.equals(trayIcon)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void removeStopButton() {
		tray.remove(stopIcon);
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Workplace.getCustomRecordingRectangle().removeRectangle();
				addApplicationIcon();
			}
		});
	}

	@Override
	public void addApplicationIcon() {
		try {
			if (!SystemTray.isSupported()) {
				logger.error("System doesnot support tray");
			}
			stage.close();
			removeAllIcon();
			java.awt.Image image = ImageIO.read(applicationImg);
			applicationIcon = new java.awt.TrayIcon(image);
			applicationIcon.setToolTip(Workplace.appName);
			// if (Workplace.getOs().equals(OperatingSystem.Window)) {
			// PopupMenu popupMenu = addExitPopup(applicationIcon);
			// }
			applicationIcon.addMouseListener(new MouseListener() {

				@Override
				public void mouseReleased(MouseEvent e) {

				}

				@Override
				public void mousePressed(MouseEvent e) {

					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							if (e.getButton() == MouseEvent.BUTTON1 && !e.isPopupTrigger()) {
								showTrayPopup(stage, e);
							}
						}
					});
				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub

				}
			});
			addToSystemTray(applicationIcon);
			Workplace.setCustomSystemTrayIcons(this);
		} catch (Exception e) {
			logger.error("Add tray icon error occured ", e);
		}
	}

	public void showTrayPopup(Stage stage, MouseEvent event) {
		if(!isTrayOpen) {
			isTrayOpen=true;
		TrayMenuPopupService trayMenuPopupService = (TrayMenuPopupService) Workplace
				.getService(BiphyServices.Tray_Menu_Popup);
		trayMenuPopupService.setEvent(event);
		Workplace.updateService(BiphyServices.Tray_Menu_Popup, trayMenuPopupService);
		trayMenuPopupService.launch();
		
		}else {
			isTrayOpen=false;

			Workplace.getCustomSystemTrayPopup().removePopup();

		}
	}

	private PopupMenu addExitPopup(TrayIcon trayIcon) {
		PopupMenu existMenu = new PopupMenu();
		MenuItem exitItem = new MenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						Platform.exit();
						System.exit(0);
					}
				});

			}
		});
		existMenu.add(exitItem);
		trayIcon.setPopupMenu(existMenu);
		return existMenu;

	}

	@Override
	public void removeApplicationIcon() {
		tray.remove(applicationIcon);
	}

	@Override
	public void addCrossHairIcon() {
		removeAllIcon();
		java.awt.Image image;

		try {
			image = ImageIO.read(crossHairImg);
			crossHairIcon = new java.awt.TrayIcon(image);
			crossHairIcon.setToolTip(Workplace.appName);
			addToSystemTray(crossHairIcon);
		} catch (IOException | AWTException e) {
			logger.error("Exception occured while adding cross hair to system tray ", e);
		}

	}

	@Override
	public void removeCrossHairIcon() {
		tray.remove(crossHairIcon);
	}

	private void removeAllIcon() {
		if (Workplace.isWindows()) {
			if (applicationIcon != null && contains(applicationIcon)) {
				removeApplicationIcon();
			}

			if (recordIcon != null && contains(recordIcon)) {
				removeRecordButton();
			}

			if (stopIcon != null && contains(stopIcon)) {
				removeStopButton();
			}

			if (crossHairIcon != null && contains(crossHairIcon)) {
				stage.hide();
				removeCrossHairIcon();
			}

		}

	}

	private void addToSystemTray(TrayIcon icon) throws AWTException {
		if (Workplace.isWindows()) {
			addExitPopup(icon);
			tray.add(icon);
		} else {
			if (tray.getTrayIcons().length == 0) {
				addPopupMenuMacOs(icon);
				tray.add(icon);
				return;
			}
			addPopupMenuMacOs(icon);
			// replacing previous icon with new icon
			for (TrayIcon trayIcon : tray.getTrayIcons()) {
				if (icon.getToolTip().equalsIgnoreCase(Workplace.appName)) {
					trayIcon.setImage(icon.getImage());
					trayIcon.setToolTip(icon.getToolTip());

					for (ActionListener listener : trayIcon.getActionListeners()) {
						trayIcon.removeActionListener(listener);
					}

					for (MouseListener listener : trayIcon.getMouseListeners()) {
						trayIcon.removeMouseListener(listener);
					}
					for (ActionListener listener : icon.getActionListeners()) {
						trayIcon.addActionListener(listener);
					}
					for (MouseListener listener : icon.getMouseListeners()) {
						trayIcon.addMouseListener(listener);
					}
				}
			}

		}

	}

	@Override
	public void addStopButton(RecordingComponent recordingComponent) {
		this.recordingComponent = recordingComponent;
		addStopButton();
	}

	public void addPopupMenuMacOs(TrayIcon icon) {

		if (Workplace.isMacOs()) {
			icon.addMouseListener(new MouseListener() {

				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mousePressed(MouseEvent e) {
					if (e.getButton() == MouseEvent.BUTTON3) {
						showMacExitMenu(e);
					}
				}

				@Override
				public void mouseExited(MouseEvent e) {

				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub

				}
			});
		}

	}

	private void addNativeHandler(MouseEvent source) {
		if (systemTrayNativeMouseHandler == null) {
			systemTrayNativeMouseHandler = new SystemTrayNativeMouseHandler();
		}
		try {
			LogManager.getLogManager().reset();

			// Get the logger for "org.jnativehook" and set the level to off.
			java.util.logging.Logger logger = java.util.logging.Logger
					.getLogger(GlobalScreen.class.getPackage().getName());
			logger.setLevel(java.util.logging.Level.OFF);
			GlobalScreen.setEventDispatcher(new JavaFxDispatchService());
			GlobalScreen.registerNativeHook();
			systemTrayNativeMouseHandler.setSource(source);
			GlobalScreen.addNativeMouseListener(systemTrayNativeMouseHandler);
		} catch (NativeHookException ex) {
			logger.error("Error occured while adding native hook", ex);
		}
	}

	private void removeNativeHandler() {
		try {
			GlobalScreen.removeNativeMouseListener(systemTrayNativeMouseHandler);
			GlobalScreen.unregisterNativeHook();
		} catch (NativeHookException e) {
			logger.error("Error occured while removing native hook", e);
		}
	}

	private void createMacExitMenu() {
		if (systemTrayNativeMouseHandler != null) {
			systemTrayNativeMouseHandler = new SystemTrayNativeMouseHandler();
		}
		jPopupMenu = new JPopupMenu("Exit");
		JMenuItem jMenuItem = new JMenuItem("Exit");

		jMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						Platform.exit();
						System.exit(0);
					}
				});

			}
		});
		jPopupMenu.add(jMenuItem);
		jPopupMenu.setInvoker(jPopupMenu);
	}

	private void showMacExitMenu(MouseEvent event) {
		if (jPopupMenu != null) {
			Thread showMacExitPopup = new Thread(new Runnable() {

				@Override
				public void run() {
					jPopupMenu.setLocation(event.getLocationOnScreen());
					jPopupMenu.setVisible(true);
					addNativeHandler(event);
				}
			});
			showMacExitPopup.start();

		}
	}

	private void hideMacExitMenu() {
		if (jPopupMenu != null) {
			Thread hideMacExitPopup = new Thread(new Runnable() {

				@Override
				public void run() {
					jPopupMenu.setVisible(false);
					removeNativeHandler();
				}
			});
			hideMacExitPopup.start();

		}
	}

	private boolean exitMenuClicked(NativeMouseEvent event, JPopupMenu popupMenu) {

		java.awt.Rectangle rectangle = new java.awt.Rectangle(popupMenu.getLocationOnScreen().x,
				popupMenu.getLocationOnScreen().y, popupMenu.getWidth(), popupMenu.getHeight());

		if (rectangle.contains(new Point(event.getX(), event.getY()))) {
			return true;
		}

		return false;
	}

	private class SystemTrayNativeMouseHandler implements NativeMouseListener {

		private MouseEvent source;

		public void setSource(MouseEvent source) {
			this.source = source;
		}

		@Override
		public void nativeMouseClicked(NativeMouseEvent arg0) {

		}

		@Override
		public void nativeMousePressed(NativeMouseEvent event) {
			if (jPopupMenu != null) {
				if (jPopupMenu.isVisible() && !exitMenuClicked(event, jPopupMenu)) {
					hideMacExitMenu();
				}
			}

		}

		@Override
		public void nativeMouseReleased(NativeMouseEvent arg0) {
			// TODO Auto-generated method stub

		}
	}

}
