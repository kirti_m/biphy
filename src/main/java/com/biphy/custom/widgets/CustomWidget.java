package com.biphy.custom.widgets;

import javafx.scene.Scene;
import javafx.stage.Stage;

public abstract class CustomWidget {

	protected Scene scene;

	protected Stage stage;

	public CustomWidget(Scene scene, Stage stage) {
		super();
		this.scene = scene;
		this.stage = stage;
	}

	public abstract void notifyListener();

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

}
