package com.biphy.custom.widgets;

import com.biphy.event.listener.CustomSystemTrayIconsListener;
import com.biphy.event.listener.RectangleListener;
import com.biphy.event.listener.SceneListener;

import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class CustomScene extends CustomWidget implements SceneListener {

	protected Pane pane;

	protected CustomRectangle customRectangle;

	private CustomSystemTrayIconsListener systemTrayIconsListener;

	public CustomScene(Scene scene, Stage stage, Pane pane) {
		super(scene, stage);
		this.pane = pane;

		// TODO Auto-generated constructor stub
	}

	public Pane getPane() {
		return pane;
	}

	public void setPane(Pane pane) {
		this.pane = pane;
	}

	public CustomRectangle getCustomRectangle() {
		return customRectangle;
	}

	public void setCustomRectangle(CustomRectangle customRectangle) {
		this.customRectangle = customRectangle;
	}

	public CustomSystemTrayIconsListener getSystemTrayIconsListener() {
		return systemTrayIconsListener;
	}

	public void setSystemTrayIconsListener(CustomSystemTrayIconsListener systemTrayIconsListener) {
		this.systemTrayIconsListener = systemTrayIconsListener;
	}

	protected void removeById(String id) {
		if (pane != null && pane.getChildren() != null) {
			Node removingNode = null;
			for (Node node : pane.getChildren()) {
				if (node.getId() != null) {
					if (node.getId().equals(id)) {
						removingNode = node;
					}
				}
			}
			pane.getChildren().remove(removingNode);
		}
	}

	public void updateScene(Rectangle rect) {
		if (pane.getChildren().size() == 0 && rect != null) {
			pane.getChildren().add(rect);
		} else if (rect != null) {
			pane.getChildren().clear();
			pane.getChildren().add(rect);
		}
		stage.setScene(scene);
	}

	public void removeAllHandler() {
		scene.setOnMousePressed(null);
		scene.setOnMouseDragged(null);
		scene.setOnMouseReleased(null);
	}

	public void notifyListener() {
		// TODO Auto-generated method stub

	}

	public void addRectangle(Rectangle rect) {
		customRectangle = new CustomRectangle(scene, stage, pane);
		customRectangle.setRectangle(rect);
	}

	public void showRecordIcon(RectangleListener rectangle) {
		systemTrayIconsListener.addRecordButton(rectangle);
	}

	@Override
	public void updateCursor(Cursor cursor) {
		if (scene != null) {
			scene.setCursor(cursor);
		}
	}

}
