package com.biphy.custom.widgets;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.biphy.model.BiphyRecord;
import com.biphy.util.ResourceUtil;
import com.biphy.util.progress.ProgressBarListener;
import com.biphy.workplace.Workplace;

import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.effect.*;
import javafx.scene.paint.*;


public class CustomNotification implements ProgressBarListener {

	private static CustomNotification customNotification;

	private Label text;

	private Stage stage;

	private Scene scene;

	private HBox mainPane;

	private StackPane stackPane;

	private StackPane imageView;

	private StackPane deleteIconView;

	private StackPane closeButtonPane;

	private BiphyRecord record;

	private Logger logger = Logger.getLogger(CustomNotification.class);

	private CustomNotification(String title, String text) {
		super();
		addText(text);
		this.mainPane = new HBox(5);
		if (!Platform.isFxApplicationThread()) {
			Platform.runLater(() -> {
				this.stage = new Stage(StageStyle.TRANSPARENT);
				this.stage.getIcons()
						.add(new Image(ResourceUtil.getImage("biphy-tray-icon-purple-32x32.png").toExternalForm()));
			});
		} else {
			this.stage = new Stage(StageStyle.TRANSPARENT);
			this.stage.getIcons()
					.add(new Image(ResourceUtil.getImage("biphy-tray-icon-purple-32x32.png").toExternalForm()));
		}

	}

	public static synchronized CustomNotification getCustomNotification(String title, String text) {
		customNotification = new CustomNotification(title, text);
		return customNotification;
	}

	@Override
	public void counterChanged(double delta) {
	}

	public void addText(String Text) {
		stackPane = new StackPane();
		this.text = new Label(Text);
		stackPane.getChildren().add(text);
		stackPane.setStyle("-fx-font-weight: bold");
		stackPane.setOnMousePressed(m -> {
			if (this.record != null && record.getRemoteFilePath() != null) {
				try {
					Desktop.getDesktop().browse(new URI(record.getRemoteFilePath()));
				} catch (IOException | URISyntaxException e) {
					AlertDialog dialog = new AlertDialog(stage, "Error", "Error occured while opening url in browser");
					dialog.showWidget(false);
					logger.error("Error occured while opening url : ", e);
				}
			}
		});
		stackPane.setPadding(new Insets(0, 5, 10, 5));
	}

	public void addGraphic(Image image) {
		this.imageView = new StackPane();
		ImageView imageViewer = new ImageView(image);
		imageViewer.setFitWidth(50);
		imageViewer.setFitHeight(40);
		imageView.getChildren().add(imageViewer);
		imageView.setPadding(new Insets(7, 5, 2, 10));
	imageView.setEffect(new DropShadow (BlurType.GAUSSIAN,Color.PURPLE,1, 2, 0, 0));
	}

	public CustomNotification build() {
		mainPane.getChildren().add(imageView);
		mainPane.getChildren().add(stackPane);

		Separator seprator = new Separator(Orientation.VERTICAL);
		seprator.setHalignment(HPos.RIGHT);
		seprator.setMinSize(1, 10);
		seprator.setStyle("-fx-background-color:#C8C8C8;");
		mainPane.getChildren().add(seprator);
		
		mainPane.setMargin(seprator, new Insets(0,0,0,20));
		mainPane.getChildren().add(deleteIconView);
		mainPane.getChildren().add(closeButtonPane);
		mainPane.setPadding(new Insets(0, 2, 2, 10));
		mainPane.setId("notification_bar");
		stage.setAlwaysOnTop(true);
		this.scene = new Scene(mainPane, 300, 70);
		scene.setFill(null);
		scene.setUserAgentStylesheet(ResourceUtil.getCss("tray_menu_popup.css").toExternalForm());
		stage.setScene(scene);
		setLocation(stage);
		stage.show();
		stage.requestFocus();
		setTimer(10000L);
		return this;
	}

	private void addCloseButton() {
		closeButtonPane = new StackPane();
		Label close = new Label("x");
		closeButtonPane.setOnMousePressed(m -> {
			stage.close();
		});
		closeButtonPane.setPadding(new Insets(0, 10, 50, 2));
		closeButtonPane.getChildren().add(close);
	}

	public void changeText(String changedText) {
		Platform.runLater(() -> {
			text.setText(changedText);
		});
	}

	public void addDeleteOption(BiphyRecord record) {
		this.record = record;
		Image deleteIcon = new Image(ResourceUtil.getImage("delete-32x32.png").toString());
		deleteIconView = new StackPane();
		ImageView deleteIconImageView = new ImageView(deleteIcon);
		deleteIconView.setPadding(new Insets(8, 5, 0, 2));
		deleteIconView.getChildren().add(deleteIconImageView);
		deleteIconView.setOnMousePressed(m -> {
			stage.close();
			Workplace.getWorkplace().getBiphyRecords().removeBiphyRecord(record);
		});
		addCloseButton();
	}

	private void setLocation(Stage stage) {
		Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
		Rectangle winSize = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
		int taskBarHeight = scrnSize.height - winSize.height;
		double x = 0, y = 0;
		if (Workplace.isWindows()) {
			x = winSize.getBounds().getMaxX() - scene.getWidth();
			y = winSize.getBounds().getMaxY() - (scene.getHeight() + taskBarHeight);
		} else {
			 x = winSize.getBounds().getMaxX() - scene.getWidth();
			 y = winSize.getBounds().getMinY() + taskBarHeight;
		}
		stage.setX(x);
		stage.setY(y);
	}

	private void setTimer(long timer) {
		Timer timerMaker = new Timer();
		TimerTask timerTask = new TimerTask() {

			@Override
			public void run() {
				Platform.runLater(() -> {
					stage.close();
					stage = null;
				});

			}
		};
		timerMaker.schedule(timerTask, timer);
	}

}
