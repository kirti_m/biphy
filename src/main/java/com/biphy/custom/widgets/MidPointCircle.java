package com.biphy.custom.widgets;

import java.util.UUID;

import com.biphy.event.handler.MidPointCircleHandler;
import com.biphy.event.handler.MidPointCircleHandler.MidPointCircleMouseDraggedHandler;
import com.biphy.event.handler.MidPointCircleHandler.MidPointCircleMouseReleasedHandler;
import com.biphy.event.listener.MidPointCircleListener;
import com.biphy.event.listener.RectangleListener;

import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class MidPointCircle extends CustomScene implements MidPointCircleListener {

	private Point2D coordinates;

	private Circle midPointCircle;

	private String id;

	private MidPointCircleMouseDraggedHandler mouseDraggedHandler;

	private MidPointCircleMouseReleasedHandler mouseReleasedHandler;

	public MidPointCircle(Scene scene, Stage stage, Pane pane, Point2D coordinates) {
		super(scene, stage, pane);
		this.coordinates = coordinates;
		this.id = UUID.randomUUID().toString();

	}

	public Point2D getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Point2D coordinates) {
		this.coordinates = coordinates;
	}

	public Circle getMidPointCircle() {
		return midPointCircle;
	}

	public void setMidPointCircle(Circle midPointCircle) {
		this.midPointCircle = midPointCircle;
	}

	public void createMidPointCircle(RectangleListener rectangleListener) {
		Circle circle = new Circle(coordinates.getX(), coordinates.getY(), 10);
		circle.setId(id);
		circle.setFill(Color.rgb(0, 0, 0, 1));
		addEventHandler(circle, rectangleListener);
		pane.getChildren().add(circle);
		this.setMidPointCircle(circle);
	}

	public void transform(double x, double y) {

	}

	private void addEventHandler(Circle circle, RectangleListener rectangleListener) {
		MidPointCircleHandler midPointCircleHandler = new MidPointCircleHandler(rectangleListener);
		midPointCircleHandler.setMidPointCircle(this);
		mouseDraggedHandler = midPointCircleHandler.new MidPointCircleMouseDraggedHandler();
		mouseReleasedHandler = midPointCircleHandler.new MidPointCircleMouseReleasedHandler();
		circle.setOnMousePressed(midPointCircleHandler.new MidPointCircleMousePressedHandler());
		circle.setOnDragDetected(mouseDraggedHandler);
		circle.setOnMouseReleased(mouseReleasedHandler);
	}


	public void midCircleDragged(Point2D oldPosition, Point2D newPosition, RectangleListener rectangleListener) {
		this.coordinates = newPosition;
		removeById(id);
		createMidPointCircle(rectangleListener);
	}


	public void addToSceneListener() {
		scene.setOnDragDetected(mouseDraggedHandler);
		scene.setOnMouseReleased(mouseReleasedHandler);
	}


	public void removeSceneListener() {
		scene.setOnMouseDragged(null);
		scene.setOnMouseReleased(null);
	}


	public void updateCursor(Cursor cursor) {
       scene.setCursor(cursor);
	}


}
