package com.biphy.custom.widgets;

import java.util.UUID;

import org.apache.log4j.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import com.biphy.custom.widgets.CustomRectangle.RectPoints;
import com.biphy.event.handler.RectangleEdgesHandler;
import com.biphy.event.handler.RectangleEdgesHandler.RectangleEdgesMouseDraggedHandler;
import com.biphy.event.handler.RectangleEdgesHandler.RectangleEdgesMouseReleasedHandler;
import com.biphy.event.handler.RectangleEdgesHandler.RectangleEdgesNativeMouseHandler;
import com.biphy.event.listener.EdgesCircleListnener;
import com.biphy.event.listener.RectangleListener;
import com.biphy.util.JavaFxDispatchService;

import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;

public class EdgesCircle extends CustomScene implements EdgesCircleListnener {

	private RectPoints coordinates;

	private Shape circle;

	private String id;

	private RectangleEdgesMouseDraggedHandler rectangleEdgesDraggedHandler;

	private RectangleEdgesMouseReleasedHandler rectangleEdgesReleasedHandler;

	private RectangleEdgesNativeMouseHandler rectangleEdgesNativeMouseHandler;

	private RectangleListener rectangleListener;

	private Logger logger = Logger.getLogger(EdgesCircle.class);

	public EdgesCircle(Scene scene, Stage stage, Pane pane) {
		super(scene, stage, pane);
	}

	public EdgesCircle(Scene scene, Stage stage, Pane pane, RectPoints coordinates) {
		super(scene, stage, pane);
		this.coordinates = coordinates;
		id = UUID.randomUUID().toString();
	}

	public RectPoints getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(RectPoints coordinates) {
		this.coordinates = coordinates;
	}

	public void createCircle(RectangleListener rectangleListener) {
		this.rectangleListener = rectangleListener;
		Circle bigCircle = new Circle(coordinates.getCoordinates().getX(), coordinates.getCoordinates().getY(), 7);
		Circle smallCircle = new Circle(coordinates.getCoordinates().getX(), coordinates.getCoordinates().getY(), 3.3);
		Shape circle = Shape.subtract(bigCircle, smallCircle);
		circle.setFill(Color.web("#7140aa"));
		circle.setId(coordinates.toString());
		addEventHandler(circle,rectangleListener);
		pane.getChildren().add(circle);
	    this.circle = circle;
	}

	public void createCircleWithoutEventHandler(RectangleListener rectangleListener) {
		this.rectangleListener = rectangleListener;
		Circle bigCircle = new Circle(coordinates.getCoordinates().getX(), coordinates.getCoordinates().getY(), 7);
		Circle smallCircle = new Circle(coordinates.getCoordinates().getX(), coordinates.getCoordinates().getY(), 3.3);
		Shape circle = Shape.subtract(bigCircle, smallCircle);
		circle.setFill(Color.web("#7140aa"));
		circle.setId(coordinates.toString());
		pane.getChildren().add(circle);
		this.circle = circle;
	}

	public void transform(double x, double y) {
	}

	private void addEventHandler(Shape circle, RectangleListener rectangleListener) {
		RectangleEdgesHandler handler = new RectangleEdgesHandler(this, coordinates);
		rectangleEdgesDraggedHandler = handler.new RectangleEdgesMouseDraggedHandler();
		rectangleEdgesReleasedHandler = handler.new RectangleEdgesMouseReleasedHandler();
		circle.setOnMousePressed(handler.new RectangleEdgesMousePressedHandler());
		rectangleEdgesNativeMouseHandler = handler.new RectangleEdgesNativeMouseHandler();
		circle.setOnDragDetected(rectangleEdgesDraggedHandler);
		circle.setOnMouseReleased(rectangleEdgesReleasedHandler);
	}


	public void addToSceneHandler() {
		scene.setOnDragDetected(rectangleEdgesDraggedHandler);
		scene.setOnMouseReleased(rectangleEdgesReleasedHandler);
	}

	public void removeFromSceneHandler() {
		scene.setOnDragDetected(null);
		scene.setOnMouseReleased(null);
	}

	public void updateCursor(Cursor cursor, boolean update) {

		if (update) {
			switch (coordinates) {
			case A:
				scene.setCursor(Cursor.MOVE);
				break;
			case B:
				scene.setCursor(Cursor.MOVE);

				break;
			case C:
				scene.setCursor(Cursor.MOVE);

				break;
			case D:
				scene.setCursor(Cursor.MOVE);
				break;

			default:
				break;
			}
		} else {
			scene.setCursor(Cursor.DEFAULT);
		}

	}

	@Override
	public void addNativeHandler() {
		try {
			GlobalScreen.setEventDispatcher(new JavaFxDispatchService());
			GlobalScreen.registerNativeHook();
			GlobalScreen.addNativeMouseMotionListener(rectangleEdgesNativeMouseHandler);
		} catch (NativeHookException ex) {
			logger.error("There was a problem registering the native hook.", ex);
		}
	}

	@Override
	public void removeNativeHandler() {
		try {
			GlobalScreen.removeNativeMouseMotionListener(rectangleEdgesNativeMouseHandler);
			GlobalScreen.unregisterNativeHook();
		} catch (NativeHookException e) {
			logger.error("There was a problem unregisterting the native hook.", e);
		}
	}

	@Override
	public Shape getCircle() {
		// TODO Auto-generated method stub
		return this.circle;
	}

	@Override
	public void removeEventHandlers() {
		circle.setOnMousePressed(null);
		circle.setOnMouseDragged(null);
		circle.setOnMouseReleased(null);
		removeNativeHandler();
	}

	@Override
	public void relocateCircle(double x, double y) {
		circle.relocate(x, y);
		this.coordinates.setCoordinates(new Point2D(x, y));
	}

	@Override
	public void removeCircle() {
		pane.getChildren().remove(circle);
		removeFromSceneHandler();
	}

	@Override
	public void notifyResize(RectPoints rectPoint, Point2D coordinate, boolean isDragged) {
		pane.getChildren().remove(circle);
		this.coordinates.setCoordinates(coordinate);
		createCircle(rectangleListener);
		if(isDragged){
			rectangleListener.notifyDrag(rectPoint, rectPoint.getCoordinates(), coordinate);
		}else{
			rectangleListener.notifyResize(rectPoint, rectPoint.getCoordinates(), coordinate);
		}
	}
}
