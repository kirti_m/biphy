package com.biphy.custom.widgets;

import com.biphy.util.ResourceUtil;

import javafx.event.EventHandler;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertDialog {

	private Stage stage;

	private String title;

	private String message;

	public AlertDialog(Stage stage, String title, String message) {
		super();
		this.stage = stage;
		this.title = title;
		this.message = message;
	}

	public void showWidgetAndCloseApplication() {
		stage.hide();
		stage.close();
		final Stage dialog = new Stage();
		dialog.getIcons().add(new Image(ResourceUtil.getImage("biphy-tray-icon-purple-32x32.png").toExternalForm()));
		dialog.setTitle(title);
		Button yes = new Button("OK");

		Label displayLabel = new Label(message);
		displayLabel.setFont(Font.font(null, FontWeight.NORMAL, 14));
		displayLabel.setMinWidth(50);

		yes.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
		dialog.initModality(Modality.NONE);
		// dialog.initOwner((Stage) tableview.getScene().getWindow());

		VBox dialogHbox = new VBox(20);
		dialogHbox.setAlignment(Pos.CENTER);

		VBox dialogVbox1 = new VBox(20);
		dialogVbox1.setAlignment(Pos.CENTER_LEFT);

		VBox dialogVbox2 = new VBox(20);
		dialogVbox2.setAlignment(Pos.CENTER_RIGHT);

		dialogHbox.getChildren().add(displayLabel);
		dialogHbox.getChildren().add(yes);

		// dialogVbox2.getChildren().add(yes);

		yes.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				System.exit(0);
			}
		});

		// dialogHbox.getChildren().addAll(dialogVbox2);
		Scene dialogScene = new Scene(dialogHbox, 500, 100);
	//	dialogScene.getStylesheets().add("//style sheet of your choice");
		dialog.setScene(dialogScene);
		dialog.show();

	}
	
	public void showWidgetAndCloseApplication(double width , double height) {
		stage.hide();
		stage.close();
		final Stage dialog = new Stage();
		dialog.getIcons().add(new Image(ResourceUtil.getImage("biphy-tray-icon-purple-32x32.png").toExternalForm()));
		dialog.setTitle(title);
		Button yes = new Button("OK");

		Label displayLabel = new Label(message);
		displayLabel.setFont(Font.font(null, FontWeight.NORMAL, 14));
		displayLabel.setMinWidth(50);

		yes.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
		dialog.initModality(Modality.NONE);
		// dialog.initOwner((Stage) tableview.getScene().getWindow());

		VBox dialogHbox = new VBox(20);
		dialogHbox.setAlignment(Pos.CENTER);

		VBox dialogVbox1 = new VBox(20);
		dialogVbox1.setAlignment(Pos.CENTER_LEFT);

		VBox dialogVbox2 = new VBox(20);
		dialogVbox2.setAlignment(Pos.CENTER_RIGHT);

		dialogHbox.getChildren().add(displayLabel);
		dialogHbox.getChildren().add(yes);

		// dialogVbox2.getChildren().add(yes);

		yes.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				System.exit(0);
			}
		});

		// dialogHbox.getChildren().addAll(dialogVbox2);
		Scene dialogScene = new Scene(dialogHbox, width,height);
	//	dialogScene.getStylesheets().add("//style sheet of your choice");
		dialog.setScene(dialogScene);
		dialog.show();

	}

	public void showWidget(boolean closeSystemOnExit) {
		stage.hide();
		stage.close();
		final Stage dialog = new Stage();
		dialog.getIcons().add(new Image(ResourceUtil.getImage("biphy-tray-icon-purple-32x32.png").toExternalForm()));
		dialog.setTitle(title);
		Button yes = new Button("OK");

		Label displayLabel = new Label(message);
		displayLabel.setFont(Font.font(null, FontWeight.NORMAL, 14));
		displayLabel.setMinWidth(50);

		yes.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
		dialog.initModality(Modality.NONE);
		// dialog.initOwner((Stage) tableview.getScene().getWindow());

		VBox dialogHbox = new VBox(20);
		dialogHbox.setAlignment(Pos.CENTER);

		VBox dialogVbox1 = new VBox(20);
		dialogVbox1.setAlignment(Pos.CENTER_LEFT);

		VBox dialogVbox2 = new VBox(20);
		dialogVbox2.setAlignment(Pos.CENTER_RIGHT);

		dialogHbox.getChildren().add(displayLabel);
		dialogHbox.getChildren().add(yes);

		// dialogVbox2.getChildren().add(yes);

		yes.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if(closeSystemOnExit){
					System.exit(0);
				}
				dialog.close();
			}
		});

		// dialogHbox.getChildren().addAll(dialogVbox2);
		Scene dialogScene = new Scene(dialogHbox, 500, 100);
		dialogScene.getStylesheets().add("//style sheet of your choice");
		dialog.setScene(dialogScene);
		dialog.show();

	}

}
