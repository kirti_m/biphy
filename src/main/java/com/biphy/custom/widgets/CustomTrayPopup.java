package com.biphy.custom.widgets;

import java.awt.Cursor;
import java.awt.event.MouseEvent;

import java.net.MalformedURLException;
import java.util.List;
import javafx.scene.control.Label;

import org.apache.log4j.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.UserDataHandler;

import com.biphy.event.handler.TrayMenuPopupHandler;
import com.biphy.event.listener.TrayMenuPopupListener;
import com.biphy.model.BiphyRecord;
import com.biphy.model.BiphyTrayHeader;
import com.biphy.model.BiphyTrayRecord;
import com.biphy.model.TrayItems;
import com.biphy.util.FileUtil;
import com.biphy.util.ResourceUtil;
import com.biphy.util.TrayPopupUtil;
import com.biphy.workplace.Workplace;
import javafx.scene.control.TextArea;
import javafx.scene.text.*;
import javafx.scene.canvas.*;
import javafx.scene.paint.Color;

import javafx.*;
import javafx.geometry.Pos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;

public class CustomTrayPopup extends CustomWidget implements TrayMenuPopupListener {

	private VBox mainPane;

	private VBox labelPane;

	private ScrollPane scrollPane;

	private ObservableList<TrayItems> trayItems;

	private ListView<TrayItems> listView;

	private TextArea textArea;

	private Rectangle2D screenSize = Screen.getPrimary().getBounds();

	private Logger logger = Logger.getLogger(CustomTrayPopup.class);

	private Point2D eventPoint2d;

	private MouseEvent event;

	private boolean showPopup;

	public CustomTrayPopup(Scene scene, Stage stage) {
		super(scene, stage);
	}

	@Override
	public void notifyListener() {

	}

	public void initializePanel() {
		System.out.println("---Panel Initialized");
		mainPane = new VBox(5);
		createHeader(mainPane);
		scrollPane = new ScrollPane();
		if (Workplace.isWindows()) {
			mainPane.setId("root-win");
		} else if (Workplace.isMacOs()) {
			mainPane.setId("root-mac");
		}
		// will be changes based on os
		mainPane.setPrefSize(320, 300);
		// mainPane.setPadding(new Insets(0, 0, 5, 0));
		Group group = new Group();
		trayItems = FXCollections.observableArrayList();
		listView = new ListView<>();
		listView.setId("listView");
		listView.setFixedCellSize(70);
		listView.setPrefSize(300, 330);
		listView.setMaxHeight(330);
		listView.setBackground(null);
		createCells();
		listView.setItems(getBiphyItems());
		scene = new Scene(mainPane, 320, 370);
		scrollPane.setPrefSize(300, 300);
		// scrollPane.setFitToHeight(true);
		scrollPane.setFitToWidth(true);
		scrollPane.setPannable(true);
		scrollPane.prefWidthProperty().bind(listView.widthProperty());
		scrollPane.prefHeightProperty().bind(listView.heightProperty());
		scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		scene.setUserAgentStylesheet(ResourceUtil.getCss("tray_menu_popup.css").toExternalForm());

		listView.setOrientation(Orientation.VERTICAL);
		// mainPane.getChildren().add(scrollPane);
		mainPane.getStyleClass().add("main-pane");
		stage.setHeight(400);
		scene.setFill(null);
		stage.setAlwaysOnTop(true);
		stage.setMaxWidth(320);
		stage.setScene(scene);

		addGlobalMouseEventTracker();

	}

	public void createWidget(Point2D eventPointer, MouseEvent e) {

		if (showPopup) {
			return;
		}
System.out.println("in create widget");
		eventPoint2d = eventPointer;
		event = e;
		// mainPane = new VBox(5);
		// createHeader(mainPane);
		// scrollPane = new ScrollPane();
		// if (Workplace.isWindows()) {
		// mainPane.setId("root-win");
		// } else if (Workplace.isMacOs()) {
		// mainPane.setId("root-mac");
		// }
		// // will be changes based on os
		// mainPane.setPrefSize(320, 300);
		//// mainPane.setPadding(new Insets(0, 0, 5, 0));
		// Group group = new Group();
		// trayItems = FXCollections.observableArrayList();
		// listView = new ListView<>();
		// listView.setId("listView");
		// listView.setFixedCellSize(70);
		// listView.setPrefSize(300, 330);
		// listView.setMaxHeight(330);
		// listView.setBackground(null);
		// createCells();
		// listView.setItems(getBiphyItems());
		// scene = new Scene(mainPane, 320, 370);
		// scrollPane.setPrefSize(300, 300);
		// // scrollPane.setFitToHeight(true);
		// scrollPane.setFitToWidth(true);
		// scrollPane.setPannable(true);
		// scrollPane.prefWidthProperty().bind(listView.widthProperty());
		// scrollPane.prefHeightProperty().bind(listView.heightProperty());
		// scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		// scene.setUserAgentStylesheet(ResourceUtil.getCss("tray_menu_popup.css").toExternalForm());
		mainPane = getContent(listView, scrollPane, mainPane, false);

		Point2D newPoint = TrayPopupUtil.calculateRectangle(new Point2D(eventPointer.getX(), eventPointer.getY()), 320,
				420, Workplace.getOs());
		// mainPane = getContent(listView, scrollPane, mainPane,false);
		//
		// listView.setOrientation(Orientation.VERTICAL);
		// // mainPane.getChildren().add(scrollPane);
		// mainPane.getStyleClass().add("main-pane");
		// stage.setX(newPoint.getX());
		stage.setY(newPoint.getY());
		stage.setWidth(320);
		// stage.setHeight(400);
		// scene.setFill(null);
		// stage.setAlwaysOnTop(true);
		// stage.setMaxWidth(320);
		// stage.setScene(scene);
		stage.show();
		// addGlobalMouseEventTracker();
		showPopup = true;
		Workplace.setCustomSystemTrayPopup(this);
	}

	// read from download files gifs created
	private ObservableList getBiphyItems() {
		List<BiphyRecord> recordList = Workplace.getWorkplace().getBiphyRecords().getBiphyRecords();
		System.out.println("record  items size-------" + recordList.size());

		for (BiphyRecord biphyRecord : recordList) {
			try {
				TrayItems trayItem = TrayPopupUtil.getTrayItemFromBiphyRecord(biphyRecord);
				if (trayItem != null) {
					trayItems.add(TrayPopupUtil.getTrayItemFromBiphyRecord(biphyRecord));
				}
			} catch (MalformedURLException e) {
				logger.error("Get Biphy Items : ", e);
			}
		}
		return trayItems;
	}

	private void createCells() {
		listView.setCellFactory(new Callback<ListView<TrayItems>, ListCell<TrayItems>>() {

			@Override
			public ListCell<TrayItems> call(ListView<TrayItems> param) {
				ListCell<TrayItems> cell = new ListCell<TrayItems>() {
					protected void updateItem(TrayItems trayItems, boolean arg1) {
						super.updateItem(trayItems, arg1);
						if (trayItems != null) {
							VBox box = null;
							if (trayItems.isBifyRecords()) {
								BiphyTrayRecord biphyTrayRecord = (BiphyTrayRecord) trayItems.getTrayItem();

								box = TrayPopupUtil.createBiphyTrayItems(biphyTrayRecord);
								setUserData(biphyTrayRecord);
							}
							setGraphic(box);
							setHeight(10);
						}
					};
				};
				return cell;
			}
		});
	}

	@Override
	public void removePopup() {
		if (scrollPane.getContent() != null) {
			scrollPane.setContent(null);
		}
		stage.close();
		showPopup = false;
		Workplace.getStage().close();
	}

	public void addGlobalMouseEventTracker() {
		// if (Workplace.isWindows()) {
		try {
			GlobalScreen.registerNativeHook();
		} catch (NativeHookException e) {
			logger.error("addGlobalMouseEventTracker nativehookexception: ", e);
		}

		TrayMenuPopupHandler trayMenuPopupHandler = new TrayMenuPopupHandler(stage, this);
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(GlobalScreen.class.getPackage().getName());
		logger.setLevel(java.util.logging.Level.OFF);
		GlobalScreen.addNativeMouseListener(trayMenuPopupHandler);
		// } else if (Workplace.isMacOs()) {
		stage.focusedProperty().addListener((obs, wasFocused, isFocused) -> {
			if (!stage.isFocused()) {
				removePopup();
			}
		});
		// }
	}

	public void showPopup(Point2D point, MouseEvent event) {
		if (mainPane != null && scrollPane != null && listView != null && stage != null) {
			Point2D newPoint = TrayPopupUtil.calculateRectangle(new Point2D(event.getX(), event.getY()), 320, 400,
					Workplace.getOs());
			listView.getItems().clear();
			listView.setItems(getBiphyItems());
			mainPane = getContent(listView, scrollPane, mainPane, true);
			stage.setX(newPoint.getX());
			stage.setY(newPoint.getY());
			stage.setWidth(320);
			stage.setHeight(400);
			scene.setFill(null);
			stage.setAlwaysOnTop(true);
			stage.setMaxWidth(320);
			stage.setScene(scene);
			stage.show();
			addGlobalMouseEventTracker();
			showPopup = true;
		} else {
			System.out.println("Create widget called");
			createWidget(point, event);
		}

	}

	@Override
	public void showPopup(Point2D point) {
		if (Workplace.getStage().isShowing()) {
			Workplace.getStage().close();
		}
		createWidget(point, event);
	}

	@Override
	public synchronized void addToPopup(BiphyRecord biphyRecord) {
		try {
			TrayItems trayItem = TrayPopupUtil.getTrayItemFromBiphyRecord(biphyRecord);
			if (trayItem == null) {
				return;
			}
			trayItems.add(TrayPopupUtil.getTrayItemFromBiphyRecord(biphyRecord));
			listView.setItems(trayItems);
			createCells();
			refreshWidget();
		} catch (Exception e) {
			logger.error("add new record to popup: ", e);
		}
	}

	@Override
	public synchronized void removeRecord(BiphyRecord biphyRecord) {
System.out.println("removing record");
		try {
			TrayItems trayItem = null;
			for (TrayItems trayItems2 : trayItems) {
				if (trayItems2.isBifyRecords()) {
					BiphyTrayRecord trayRecord = (BiphyTrayRecord) trayItems2.getTrayItem();
					BiphyRecord record = trayRecord.getRecord();
					if (record.getFileName().equals(biphyRecord.getFileName())) {
						trayItem = trayItems2;
					}
				}
			}
			trayItems.remove(trayItem);
			listView.setItems(trayItems);
			createCells();
			refreshWidget();
			if(trayItems.size()<=0) {
				mainPane=getContent(listView, scrollPane, mainPane, true);
			}
			FileUtil.deleteBiphyRecord(biphyRecord);
		} catch (Exception e) {
			logger.error("remove record from popup: ", e);
		}

	}

	private void refreshWidget() {
		if (eventPoint2d != null) {
			removePopup();
			showPopup(eventPoint2d);
		}
		
	}

	private void createHeader(Pane pane) {
		BiphyTrayHeader header = new BiphyTrayHeader(stage);
		pane.getChildren().add(TrayPopupUtil.createBiphyTrayHeader(header));
	}

	private VBox getContent(ListView<TrayItems> listView, ScrollPane scrollPane, VBox mainPane, Boolean showflag) {
		mainPane.getChildren().remove(labelPane);
		// listView.getItems().setAll(getBiphyItems());
		// System.out.println(getBiphyItems().size());
		if (listView.getItems().isEmpty()) {
			mainPane.getChildren().remove(scrollPane);

			System.out.println("creating label***" + listView.getItems().size());
			labelPane = getLabelPane();

			labelPane.getChildren().clear();
			Label label = new Label();
			label.setText("Lets make some bif's.\n");
			label.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.EXTRA_BOLD, 14.0f));

			Label label2 = new Label();

			label2.setText("Use the 'new' bif button at\n");
			label2.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.EXTRA_BOLD, 14.0f));

			labelPane.setAlignment(Pos.BASELINE_LEFT);
			Label label3 = new Label();
			label3.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.EXTRA_BOLD, 14.0f));

			label3.setText("the top to get started");
			labelPane.setAlignment(Pos.BASELINE_CENTER);

			// Text t = new Text("This is a text sample");
			// t.setText("This is a text sample");
			// t.setFont(Font.font ("Verdana", 20));
			// t.setStyle("fancytext");
			// t.setFill(Color.BLACK);
			labelPane.setPrefSize(320, 300);
			labelPane.setPadding(new Insets(5, 10, 10, 5));
			labelPane.setAlignment(Pos.CENTER);
			labelPane.getChildren().add(label);
			labelPane.getChildren().add(label2);
			labelPane.getChildren().add(label3);

			mainPane.getChildren().add(labelPane);

		} else {
			System.out.println("creating list***" + showflag);

			System.out.println("in show flag");
			scrollPane.setContent(listView);

			if (!mainPane.getChildren().contains(scrollPane)) {
				System.out.println("scrollpane exists");

				mainPane.getChildren().add(scrollPane);

			}
		}
		return mainPane;
	}

	public VBox getLabelPane() {
		if (labelPane == null)
			labelPane = new VBox(5);
		return labelPane;

	}

}
