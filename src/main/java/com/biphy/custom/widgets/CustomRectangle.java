package com.biphy.custom.widgets;

import java.awt.GraphicsEnvironment;
import java.awt.Robot;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.LogManager;

import org.apache.log4j.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import com.biphy.component.RecordingComponent;
import com.biphy.event.handler.CustomRectangleHandler;
import com.biphy.event.handler.CustomRectangleHandler.CustomRectangleMouseDraggedHandler;
import com.biphy.event.handler.CustomRectangleHandler.CustomRectangleMousePressedHandler;
import com.biphy.event.handler.CustomRectangleHandler.CustomRectangleMouseReleasedHandler;
import com.biphy.event.handler.CustomRectangleHandler.CustomRectangleNativeEventHandler;
import com.biphy.event.listener.RectangleListener;
import com.biphy.util.JavaFxDispatchService;
import com.biphy.util.RectangleUtil;
import com.biphy.util.ResourceUtil;
import com.biphy.workplace.OperatingSystem;
import com.biphy.workplace.Workplace;

import javafx.application.Platform;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class CustomRectangle extends CustomScene implements RectangleListener {

	/*
	 * Here A,B,C,D are four points of rectangle A B ---------------- | | | | |
	 * | | | ---------------- D C
	 *
	 */
	public enum RectPoints {
		A(new Point2D(0, 0)), B(new Point2D(0, 0)), C(new Point2D(0, 0)), D(new Point2D(0, 0));

		private RectPoints(Point2D points) {
			this.coordinates = points;
		}

		private Point2D coordinates;

		public void setCoordinates(Point2D points) {
			this.coordinates = points;
		}

		public void setCoordinates(double x, double y) {
			this.coordinates = new Point2D(x, y);
		}

		public Point2D getCoordinates() {
			return coordinates;
		}
	}

	private Map<RectPoints, EdgesCircle> rectPoints;

	private Rectangle rectangle;

	private CustomRectangleMousePressedHandler customRectangleMousePressedHandler;

	private CustomRectangleMouseDraggedHandler customRectangleMouseDraggedHandler;

	private CustomRectangleMouseReleasedHandler customRectangleMouseReleasedHandler;

	private CustomRectangleNativeEventHandler customRectangleNativeEventHandler;

	private String id;

	private Robot robot;

	private Logger logger = Logger.getLogger(CustomRectangle.class);

	private RecordingComponent recordingComponent;

	private HBox recordingPane;

	public CustomRectangle(Scene scene, Stage stage, Pane pane) {
		super(scene, stage, pane);
		rectPoints = new HashMap<CustomRectangle.RectPoints, EdgesCircle>();
		id = UUID.randomUUID().toString();
	}

	public Rectangle getRectangle() {
		return rectangle;
	}

	public void setRectangle(Rectangle rectangle) {
		if (rectangle == null) {
			return;
		}
		pane.getChildren().clear();
		this.rectangle = new Rectangle(rectangle.getX() + rectangle.getLayoutX(),
				rectangle.getY() + rectangle.getLayoutY(), rectangle.getWidth(), rectangle.getHeight());

		pane.getChildren().add(this.rectangle);
		setRectangleCoordinates(true);
		CustomRectangleHandler customRectangleHandler = new CustomRectangleHandler(this);
		customRectangleMousePressedHandler = customRectangleHandler.new CustomRectangleMousePressedHandler();
		customRectangleMouseDraggedHandler = customRectangleHandler.new CustomRectangleMouseDraggedHandler();
		customRectangleMouseReleasedHandler = customRectangleHandler.new CustomRectangleMouseReleasedHandler();
		customRectangleNativeEventHandler = customRectangleHandler.new CustomRectangleNativeEventHandler();
		this.rectangle.setOnMousePressed(customRectangleMousePressedHandler);
		this.rectangle.setOnMouseDragged(customRectangleMouseDraggedHandler);
		this.rectangle.setOnMouseReleased(customRectangleMouseReleasedHandler);
		this.rectangle.setOnMouseEntered(customRectangleHandler.new CustomRectangleHoverHandler(true));
		this.rectangle.setOnMouseExited(customRectangleHandler.new CustomRectangleHoverHandler(false));
		this.rectangle.setId(id);
		addRecordIcon();
		Workplace.setCustomRecordingRectangle(this);
		if (Workplace.isMacOs()) {
			return;
		}
		windowsMakeStageActive(true);
	}

	public void setRectangleWithoutEventHandler(Rectangle rectangle) {
		if (rectangle == null) {
			return;
		}
		pane.getChildren().clear();
		this.rectangle = rectangle;
		pane.getChildren().add(rectangle);
		setRectangleCoordinates(false);
		rectangle.setId(id);
		Workplace.setCustomRecordingRectangle(this);
	}

	public Map<RectPoints, EdgesCircle> getRectPoints() {
		return rectPoints;
	}

	public void addRectPoint(RectPoints rect, EdgesCircle circle) {
		this.rectPoints.put(rect, circle);
	}

	public EdgesCircle getRectPoint(RectPoints rect) {
		if (this.rectPoints.containsKey(rect)) {
			return this.rectPoints.get(rect);
		}
		return null;
	}

	public Rectangle moveRectangle(double x, double y) {
		Rectangle rect = this.rectangle;
		Rectangle newRect = new Rectangle(rect.getX() + x, rect.getY() + y, rect.getWidth(), rect.getHeight());
		newRect.setFill(Color.rgb(0, 0, 0, 0.6));
		this.setRectangle(newRect);
		return rectangle;
	}

	public Rectangle resizeRectangle() {
		return rectangle;

	}

	private void updateEdges() {

	}

	public void setRectangleCoordinates(boolean withEventHandler) {
		Point2D A = new Point2D(rectangle.getX(), rectangle.getY());
		double width = rectangle.getWidth();
		double height = rectangle.getHeight();
		Point2D C = new Point2D(rectangle.getX() + width, rectangle.getY() + height);
		Point2D B = new Point2D(A.getX(), C.getY());
		Point2D D = new Point2D(C.getX(), A.getY());
		RectPoints.A.setCoordinates(A);
		RectPoints.B.setCoordinates(B);
		RectPoints.C.setCoordinates(C);
		RectPoints.D.setCoordinates(D);
		RectPoints[] rectPoints = { RectPoints.A, RectPoints.B, RectPoints.C, RectPoints.D };
		for (int i = 0; i < 4; i++) {
			EdgesCircle edgeCircle = new EdgesCircle(scene, stage, pane, rectPoints[i]);
			if (withEventHandler) {
				edgeCircle.createCircle(this);
			} else {
				edgeCircle.createCircleWithoutEventHandler(this);
			}
			this.rectPoints.put(rectPoints[i], edgeCircle);
		}
		RectangleUtil.makeRectangleTransperent(rectangle);
		addSize();
	}

	private void addSize() {
		Label label = new Label(Double.toString(Math.floor(rectangle.getWidth())) + " x "
				+ Double.toString(Math.floor(rectangle.getHeight())));
		label.setTextFill(Color.web("#C1C1C1"));
		label.setStyle("-fx-font-weight: bold");
		if (rectangle.getLayoutBounds().getMaxX() > 80 && rectangle.getLayoutBounds().getMaxY() > 20) {
			label.setLayoutX(rectangle.getLayoutBounds().getMaxX() - 80);
			label.setLayoutY(rectangle.getLayoutBounds().getMaxY() - 20);
			pane.getChildren().add(label);
		}

	}

	public void notifyMove(Point2D previousLocation, Point2D newLocation) {
		if (previousLocation != null && newLocation != null) {
			moveRectangle(newLocation.getX() - previousLocation.getX(), newLocation.getY() - previousLocation.getY());
		}
	}

	public void updateCursor(Cursor cursor) {
		scene.setCursor(cursor);
	}

	@Override
	public void notifyResize(com.biphy.custom.widgets.CustomRectangle.RectPoints direction, Point2D previousLocation,
			Point2D newLocation) {
		Rectangle resizedRect = RectangleUtil.resizeRectangleFromEdges(rectangle, direction, newLocation);
		setRectangle(resizedRect);
	}

	@Override
	public void updateEdgesCorordinate(com.biphy.custom.widgets.CustomRectangle.RectPoints direction,
			EdgesCircle edgeCircle) {
		// TODO Auto-generated method stub

	}

	private void addRecordIcon() {
		if (Workplace.isMacOs()) {
			if (this.getSystemTrayIconsListener() == null) {
				Workplace.getCustomSystemTrayIcons().setScene(scene);
				this.setSystemTrayIconsListener(Workplace.getCustomSystemTrayIcons());
			}
			showRecordIcon(this);
		} else {
			if (this.getSystemTrayIconsListener() == null) {
				Workplace.getCustomSystemTrayIcons().setScene(scene);
				this.setSystemTrayIconsListener(Workplace.getCustomSystemTrayIcons());
			}
			addRecordPaneWindows(false);

		}

	}

	@Override
	public void startRecording(Rectangle recordingRectangle) {
		Bounds screenBounds = recordingRectangle.localToScreen(recordingRectangle.getBoundsInLocal());
		if (Workplace.isWindows()) {
			pane.getChildren().clear();
			rectangle = new Rectangle(recordingRectangle.getX(), recordingRectangle.getY(),
					recordingRectangle.getWidth(), recordingRectangle.getHeight());
			setRectangleWithoutEventHandler(rectangle);
			rectangle.setFill(Color.TRANSPARENT);
			addNativeHandler();
		} else {
			logger.error("Start recording : "+recordingRectangle);
			double x = recordingRectangle.getX()+recordingRectangle.getLayoutX();
			double y = recordingRectangle.getY()+recordingRectangle.getLayoutY();
			Thread startMacRecorder = new Thread(new Runnable() {

				@Override
				public void run() {
					Workplace.getOsUtil().createMacRectangle(Math.floor(screenBounds.getMinX()),y+10, screenBounds.getWidth(), screenBounds.getHeight());
				}
			});
			startMacRecorder.start();

			removeAllHandler();
			removeNativeHandler();

			pane.getChildren().clear();

			stage.close();
			stage.setScene(null);

		}
		addRecordPaneWindows(true);
	}

	@Override
	public void removeRectangle() {
		pane.getChildren().clear();
		removeNativeHandler();
	}

	@Override
	public void addNativeHandler() {
		try {
			LogManager.getLogManager().reset();

			// Get the logger for "org.jnativehook" and set the level to off.
			java.util.logging.Logger logger = java.util.logging.Logger
					.getLogger(GlobalScreen.class.getPackage().getName());
			logger.setLevel(java.util.logging.Level.OFF);
			GlobalScreen.setEventDispatcher(new JavaFxDispatchService());
			GlobalScreen.registerNativeHook();
			GlobalScreen.addNativeKeyListener(customRectangleNativeEventHandler);
			GlobalScreen.addNativeMouseListener(customRectangleNativeEventHandler);
			GlobalScreen.addNativeMouseMotionListener(customRectangleNativeEventHandler);
		} catch (NativeHookException ex) {
			logger.error("Error occured while adding native hook", ex);
		}
	}

	@Override
	public void removeNativeHandler() {
		try {
			GlobalScreen.removeNativeKeyListener(customRectangleNativeEventHandler);
			GlobalScreen.removeNativeMouseListener(customRectangleNativeEventHandler);
			GlobalScreen.removeNativeMouseMotionListener(customRectangleNativeEventHandler);
			GlobalScreen.unregisterNativeHook();
		} catch (NativeHookException e) {
			logger.error("Error occured while removing native hook", e);
		}
	}

	@Override
	public void relocalteRectangle(Point2D previousLocation, Point2D newLocation) {
		if (newLocation != null && previousLocation != null) {
			double x = newLocation.getX() - previousLocation.getX();
			double y = newLocation.getY() - previousLocation.getY();
			Rectangle rect = this.rectangle;
			Point2D newCoordinate = new Point2D(rect.getX() + x, rect.getY() + y);
			Point2D A = new Point2D(newCoordinate.getX(), newCoordinate.getY());
			// Point2D C = new
			// Point2D(newCoordinate.getX(),screenBounds.getMaxY());
			// Point2D B = new Point2D(A.getX(), C.getY());
			// Point2D D = new Point2D(C.getX(), A.getY());
			rect.relocate(newCoordinate.getX(), newCoordinate.getY());
			for (RectPoints points : rectPoints.keySet()) {
				rectPoints.get(points).removeCircle();
			}
		}

	}

	@Override
	public void notifyDrag(RectPoints direction, Point2D previousLocation, Point2D newLocation) {
		Rectangle resizedRect = RectangleUtil.resizeRectangleFromEdges(rectangle, direction, newLocation);
		List<RectPoints> rectList = RectangleUtil.createNewRectangleEdges(resizedRect);
		rectangle.setWidth(resizedRect.getWidth());
		rectangle.setHeight(resizedRect.getHeight());
		rectangle.autosize();
		rectangle.relocate(resizedRect.getX(), resizedRect.getY());
		for (RectPoints points : rectList) {
			rectPoints.get(points).relocateCircle(points.getCoordinates().getX(), points.getCoordinates().getY());
		}

	}

	@Override
	public void windowsMakeStageActive(boolean makeFullScreeen) {
		if (makeFullScreeen) {
			stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
			stage.setFullScreen(true);
			return;
		}
		if (!stage.isShowing() || !stage.isFocused() || !stage.isAlwaysOnTop()) {
			stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
			stage.setFullScreen(true);
			stage.show();
			stage.toFront();
			// pane.setMouseTransparent(true);
			// rectangle.setMouseTransparent(true);
		}
	}

	private void addRecordPaneWindows(boolean isRecording) {
		if (recordingComponent == null) {
			recordingComponent = new RecordingComponent();
		}
		recordingPane = new HBox(10);
		ImageView stopRecordingView = null;

		Button recordingButton = new Button();
		ImageView recordingView = new ImageView(
				new Image(ResourceUtil.getImage("record-system-tray.png").toExternalForm()));
		recordingButton.setGraphic(recordingView);
		recordingButton.setOnMousePressed(m -> {
			this.startRecording(rectangle);
			recordingComponent.setRectangle(this);
			recordingComponent.startRecording();
			Workplace.getCustomSystemTrayIcons().addStopButton(recordingComponent);
		});
		recordingButton.setStyle("-fx-padding :  0px 15px 0px 15px ; ");
		recordingButton.setBackground(null);
		recordingButton.setStyle("-fx-background-color:null");

		Button stopRecodingButton = new Button();
		stopRecordingView = new ImageView(
				new Image(ResourceUtil.getImage("stop-system-tray-icon-16x16.png").toExternalForm()));
		stopRecodingButton.setGraphic(stopRecordingView);
		stopRecodingButton.setOnAction(m -> {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					removeRectangle();
					removeAllHandler();
					removeNativeHandler();
					recordingComponent.stopRecording();
					Workplace.getStage().close();
					Workplace.getCustomSystemTrayIcons().addApplicationIcon();
				}
			});
		});
		stopRecodingButton.setBackground(null);
		stopRecodingButton.setStyle("-fx-background-color:null");

		Button cancelImageButton = new Button();
		ImageView cancelImageView = new ImageView(
				new Image(ResourceUtil.getImage("cancel-button-icon-16x-16.png").toExternalForm()));
		cancelImageButton.setGraphic(cancelImageView);
		cancelImageButton.setOnAction(m -> {

			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					removeAllHandler();
					removeNativeHandler();
					removeRectangle();
					recordingComponent.cancelRecording();
					Workplace.getStage().close();
					Workplace.getCustomSystemTrayIcons().addApplicationIcon();
				}
			});
		});

		cancelImageView.setStyle("-fx-padding : 0px 15px 0px 15px ;");
		cancelImageButton.setBackground(null);
		cancelImageButton.setStyle("-fx-background-color:null");

		if (!isRecording) {
			recordingPane.getChildren().add(recordingButton);
		} else {
			recordingPane.getChildren().add(stopRecodingButton);
		}
		recordingPane.getChildren().add(cancelImageButton);
		recordingPane.setStyle("-fx-background-color: #E3E5EB ;-fx-border-style: solid outside ; -fx-border-width: 2;-fx-border-radius: 5;-fx-border-color:#D3D3D3	 ;");
		Bounds rectBounds = rectangle.getBoundsInParent();
		double x = (((rectBounds.getMinX() + rectBounds.getMaxX()) / 2) - 50);
		double y = (RectPoints.C.getCoordinates().getY() + 10);
		java.awt.Rectangle winSize = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
		recordingPane.setPadding(new Insets(0, 0, 0, 0));
		if (winSize.getMaxY() > RectPoints.C.getCoordinates().getY()) {
			recordingPane.resizeRelocate(x, y, 200, 50);
			pane.getChildren().add(recordingPane);
		} else {
			y = (RectPoints.C.getCoordinates().getY() - 50);
			recordingPane.resizeRelocate(x, y, 200, 50);
			if (!isRecording) {
				pane.getChildren().add(recordingPane);
			} else {
				pane.getChildren().remove(recordingPane);
			}

		}

	}

}