package com.biphy.custom.widgets;

import java.util.UUID;

import com.biphy.util.ResourceUtil;

import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class CustomRecordPanel extends CustomScene {

	public CustomRecordPanel(Scene scene, Stage stage, Pane pane) {
		super(scene, stage, pane);
		id = UUID.randomUUID().toString();
		// TODO Auto-generated constructor stub
	}

	private HBox hbox;

	private Button record;

	private Button cancel;

	private String id;

	public void createRecordControl(CustomRectangle rectangle) {
		removeById(id);
		Rectangle recordRect = rectangle.getRectangle();
		Point2D recordPanelPoint = new Point2D((recordRect.getX() + (recordRect.getWidth() / 2)) - 55,
				recordRect.getY() + recordRect.getHeight() + 15);
		hbox = new HBox();
		hbox.setLayoutX(recordPanelPoint.getX());
		hbox.setLayoutY(recordPanelPoint.getY());
		record = new Button();
		Image recordImage = new Image(ResourceUtil.getImage("record-button-icon.png").toString(), 20, 20, true, true);
		record.setGraphic(new ImageView(recordImage));
		record.setOnMousePressed(m -> {
			System.out.println("Record Button Pressed");
		});
		cancel = new Button();
		Image cancelImage = new Image(ResourceUtil.getImage("cancel-button-icon.png").toString(), 20, 20, true, true);
		cancel.setGraphic(new ImageView(cancelImage));
		cancel.setOnMousePressed(m -> {
			System.out.println("Cancel Button Pressed");
		});
		hbox.getChildren().add(record);
		hbox.getChildren().add(cancel);
		hbox.setSpacing(0);
		hbox.setAlignment(Pos.CENTER);
		hbox.setStyle("-fx-background-color:rgb(186,153,122);");
		hbox.setId(id);
		pane.getChildren().add(hbox);
		stage.setScene(scene);
		stage.show();

	}

}
