package com.biphy.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import com.biphy.util.ResourceUtil;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class SplashController implements Initializable {

    @FXML
    private ProgressBar progressBar;
    @FXML
    private ImageView splashImage;

    public void initialize(URL arg0, ResourceBundle arg1) {

        System.out.println("Progresss Bar : "+splashImage);


        splashImage.setImage(new Image(ResourceUtil.getImage("preview-xl.jpg").toString()));

        TimerTask timerTask = new TimerTask() {
                 public void run() {
                     progressBar.setProgress(progressBar.getProgress() + 1);
                }
            };

        Timer timer= new Timer("Timer");
        timer.scheduleAtFixedRate(timerTask, 1000, 2000);
        if(progressBar.getProgress() == 100){
            timer.cancel();
        }

}

}
