package com.biphy.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeType;

public class ShowRecorderController implements Initializable {

	@FXML
	private Rectangle rectangle;

	public void initialize(URL arg0, ResourceBundle arg1) {
		rectangle.setStroke(Color.BLACK);
		rectangle.setStrokeDashOffset(5);
		rectangle.setStrokeLineCap(StrokeLineCap.SQUARE);
		rectangle.setStrokeWidth(2);
		rectangle.setStrokeType(StrokeType.OUTSIDE);
		rectangle.setFill(new Color(255,255,255,0.8));
		// TODO Auto-generated method stub

	}

}
