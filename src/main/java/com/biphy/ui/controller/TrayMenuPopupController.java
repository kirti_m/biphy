package com.biphy.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;

import com.biphy.model.BiphyTrayRecord;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.layout.FlowPane;

public class TrayMenuPopupController implements Initializable {

	@FXML
	private FlowPane flowPane;
	@FXML
	private ListView<BiphyTrayRecord> listView;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		/*listView = new ListView<BiphyTrayRecord>();
		ObservableList<BiphyTrayRecord> trayItems = FXCollections.observableArrayList();
		trayItems.add(new BiphyTrayRecord("Untitled1.gif",
				new Image(ResourceUtil.getImage("scenery-img-1.jfif").toString()), "5 minutes ago"));

		trayItems.add(new BiphyTrayRecord("Untitled2.gif",
				new Image(ResourceUtil.getImage("scenery-img-2.jfif").toString()), "15 minutes ago"));

		listView.setCellFactory(new Callback<ListView<BiphyTrayRecord>, ListCell<BiphyTrayRecord>>() {

			@Override
			public ListCell<BiphyTrayRecord> call(ListView<BiphyTrayRecord> param) {
				ListCell<BiphyTrayRecord> cell = new ListCell<BiphyTrayRecord>() {
					protected void updateItem(BiphyTrayRecord trayItems, boolean arg1) {
						super.updateItem(trayItems, arg1);
						if (trayItems != null) {
							HBox box = new HBox(2);
							box.getChildren().add(trayItems.getThumImageView());
							VBox vBox = new VBox(3);
							Label name = new Label(trayItems.getBifName());
							Label updated = new Label(trayItems.getUploadedTime());
							vBox.getChildren().add(name);
							vBox.getChildren().add(updated);
							box.getChildren().add(vBox);
							box.getChildren().add(trayItems.getDownloadIconView());
							box.getChildren().add(trayItems.getDeleteIconView());
							setGraphic(box);
							// setBackground(null);
						}
					};
				};
				return cell;
			}
		});
		listView.setItems(trayItems);*/

	}

}
