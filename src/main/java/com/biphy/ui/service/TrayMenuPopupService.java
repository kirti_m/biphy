package com.biphy.ui.service;

import java.awt.event.MouseEvent;

import com.biphy.custom.widgets.CustomTrayPopup;
import com.biphy.main.RecorderScene;
import com.biphy.workplace.Workplace;

import javafx.geometry.Point2D;
import javafx.stage.Stage;

public class TrayMenuPopupService extends RecorderScene {

	private MouseEvent event;
	private boolean launch;

	public TrayMenuPopupService(Stage stage, MouseEvent e) {
		super(stage);
		this.event = e;
	}

	public void setEvent(MouseEvent event) {
		this.event = event;
	}

	@Override
	public void launch() {
		CustomTrayPopup trayPopup = (Workplace.getCustomSystemTrayPopup() == null ? new CustomTrayPopup(null, stage)
				: Workplace.getCustomSystemTrayPopup());
		
		trayPopup.showPopup(new Point2D(event.getXOnScreen(), event.getYOnScreen()), event);
		launch = true;
	}

	@Override
	public void nextScene(Stage stage) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isLaunched() {
		// TODO Auto-generated method stub
		return launch;
	}

}
