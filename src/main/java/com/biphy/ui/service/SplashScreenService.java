package com.biphy.ui.service;

import org.apache.log4j.Logger;

import com.biphy.main.RecorderScene;
import com.biphy.workplace.BiphyServices;
import com.biphy.workplace.Workplace;

import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class SplashScreenService extends RecorderScene {

	private boolean launch;

	private Logger logger = Logger.getLogger(SplashScreenService.class);

    public SplashScreenService(Stage stage) {
        super(stage);
    }

    @Override
    public void launch() {

        try {
          /*  BorderPane root = (BorderPane) FXMLLoader.load(ResourceUtil.getFXML("Splash.fxml"));
            Scene scene = new Scene(root, 390, 300);

            scene.getStylesheets().add(ResourceUtil.getCss("application.css").toExternalForm());
            root.setStyle("-fx-background-color: black;");*/
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.setScene(null);
            stage.show();

            nextScene(stage);
        } catch (Exception e) {
        	logger.error("Error occured in initialization of app",e);
        }

    }

    @Override
    public void nextScene(Stage stage) {
    	launch = false;
        Workplace.getService(BiphyServices.Tray_Icon).launch();
    }

	@Override
	public boolean isLaunched() {
		return launch;
	}



}
