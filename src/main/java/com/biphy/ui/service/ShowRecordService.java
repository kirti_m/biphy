package com.biphy.ui.service;

import com.biphy.custom.widgets.CustomScene;
import com.biphy.event.handler.SceneHandler;
import com.biphy.main.RecorderScene;
import com.biphy.util.ResourceUtil;
import com.biphy.workplace.BiphyServices;
import com.biphy.workplace.Workplace;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ShowRecordService extends RecorderScene {

	private Point2D mouseClicked;
	private Point2D mouseReleased;
	private Rectangle highlighRect;
	private boolean launch;

	public ShowRecordService(Stage stage) {
		super(stage);
	}

	@Override
	public void launch() {
		Rectangle2D screenSize = null;
		if (Workplace.getScreenUtil().isMultiScreenSetup()) {
			screenSize = new Rectangle2D(0, 0, Workplace.getScreenUtil().getDimension().getWidth(),
					Workplace.getScreenUtil().getDimension().getHeight());
		} else {
			screenSize = Screen.getPrimary().getBounds();
		}
		AnchorPane root = new AnchorPane();
		root.setStyle("-fx-background-color:rgb(113,64,170,0.6);");
		stage = new Stage();
		stage.setX(0);
		stage.setY(0);
		stage.getIcons().add(new Image(ResourceUtil.getImage("biphy-tray-icon-purple-32x32.png").toExternalForm()));
		stage.initStyle(StageStyle.TRANSPARENT);
		stage.setResizable(true);
		Scene scene = new Scene(root, screenSize.getWidth(), screenSize.getHeight(), Color.TRANSPARENT);

		scene.setFill(null);
		stage.setScene(scene);
		CustomScene customScene = new CustomScene(scene, stage, root);
		Workplace.getCustomSystemTrayIcons().addCrossHairIcon();

		SceneHandler sceneHandler = new SceneHandler(customScene);
		scene.setOnMousePressed(sceneHandler.new SceneMousePressedHandler());

		scene.setOnMouseDragged(sceneHandler.new SceneMouseDraggedHandler());

		scene.setOnMouseReleased(sceneHandler.new SceneMouseReleasedHandler(this, stage, customScene));
		stage.setOnCloseRequest(m -> {
			Workplace.getCustomSystemTrayIcons().addApplicationIcon();
		});

		scene.setOnKeyPressed(m -> {
			if (m.getCode() == KeyCode.ESCAPE && !Workplace.isRecording()) {
				stage.close();
				Workplace.getCustomSystemTrayIcons().addApplicationIcon();
			}
		});

		stage.show();
		launch = true;
		stage.requestFocus();
	}

	@Override
	public void nextScene(Stage stage) {
	}

	public void nextScene(Stage stage, CustomScene customScene) {
		Workplace.setStage(stage);
		Workplace.setScene(customScene.getScene());
		MapRecorderScreenService mapRecorder = (MapRecorderScreenService) Workplace
				.getService(BiphyServices.Map_Recorder_Screen);
		mapRecorder.setCustomScene(customScene);
		launch = false;
		mapRecorder.launch();

	}

	@Override
	public boolean isLaunched() {
		// TODO Auto-generated method stub
		return launch;
	}

}
