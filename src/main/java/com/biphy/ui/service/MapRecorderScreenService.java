package com.biphy.ui.service;

import com.biphy.custom.widgets.CustomScene;
import com.biphy.main.RecorderScene;
import com.biphy.workplace.Workplace;

import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class MapRecorderScreenService extends RecorderScene {

	private CustomScene customScene;

	private boolean launch;

	public MapRecorderScreenService(Stage stage) {
		super(stage);
	}

	public void setCustomScene(CustomScene customScene) {
		this.customScene = customScene;
	}

	@Override
	public void launch() {
		// setRecordPanel();
		Rectangle2D screenSize = null;
		if (Workplace.getScreenUtil().isMultiScreenSetup()) {
			screenSize = new Rectangle2D(0, 0, Workplace.getScreenUtil().getDimension().getWidth(),
					Workplace.getScreenUtil().getDimension().getHeight());
		} else {
			screenSize = Screen.getPrimary().getBounds();
		}
		stage = Workplace.getStage();
		stage.setX(0);
		stage.setY(0);
		stage.setWidth(screenSize.getWidth());
		stage.setHeight(screenSize.getHeight());
		Scene scene = customScene.getScene();
		scene.setFill(null);
		Pane root = customScene.getPane();
		root.setStyle("-fx-background-color:null");
		stage.setScene(scene);
		stage.setOnCloseRequest(m -> {
			Workplace.getCustomSystemTrayIcons().removeRecordButton();
			Workplace.getCustomSystemTrayIcons().removeStopButton();
		});
		launch = true;
	}

	@Override
	public void nextScene(Stage stage) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isLaunched() {
		return launch;
	}

}
