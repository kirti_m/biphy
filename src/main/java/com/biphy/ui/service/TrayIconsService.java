/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.biphy.ui.service;

import org.apache.log4j.Logger;

import com.biphy.custom.widgets.CustomSystemTrayIcons;
import com.biphy.main.RecorderScene;
import com.biphy.workplace.BiphyServices;
import com.biphy.workplace.Workplace;

import javafx.stage.Stage;

/**
 *
 * @author Neosoft
 */
public class TrayIconsService extends RecorderScene {


	private boolean launch;

	private Logger logger = Logger.getLogger(TrayIconsService.class);

	public TrayIconsService(Stage stage) {
		super(stage);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.screenrecorder.main.RecorderScene#launch() add the launch icon to
	 * System tray
	 */
	@Override
	public void launch() {
		CustomSystemTrayIcons trayIcons = new CustomSystemTrayIcons(null, stage);
		Workplace.setCustomSystemTrayIcons(trayIcons);
		Workplace.getCustomSystemTrayIcons().addApplicationIcon();
	}

	@Override
	public void nextScene(Stage stage) {
		launch = false;
		Workplace.getService(BiphyServices.Show_Record).launch();
	}



	@Override
	public boolean isLaunched() {
		// TODO Auto-generated method stub
		return launch;
	}

}
