package com.biphy.mac.component;import com.biphy.util.ResourceUtil;

import javafx.scene.*;
import javafx.scene.image.*;
import javafx.scene.layout.VBox;

public class Snippet {
	
	public static void main(String[] args) {
		
		VBox mainPane = new VBox(5);

		Image image = new Image(ResourceUtil.getImage("cancel-button-icon.png").toString(), 20, 20, true, true);
	
	Scene scene = new Scene(mainPane,400, 300);
	scene.setCursor(new ImageCursor(image,
	                                image.getWidth() / 2,
	                                image.getHeight() /2));
}

}