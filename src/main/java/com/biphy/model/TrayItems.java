package com.biphy.model;

public class TrayItems {

	public enum TrayItemType {
		BifyRecords, BifyHeader, BifyPanel
	}

	protected TrayItemType type;

	protected Object trayItem;

	public boolean isBifyRecords() {
		if (type == TrayItemType.BifyRecords) {
			return true;
		}
		return false;
	}

	public boolean isBifyHeader() {
		if (type == TrayItemType.BifyHeader) {
			return true;
		}
		return false;
	}

	public boolean isBifyPanel() {
		if (type == TrayItemType.BifyPanel) {
			return true;
		}
		return false;
	}

	public Object getTrayItem() {
		return trayItem;
	}


}
