package com.biphy.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class Duration {

	private static long seconds;
	private static long minutes;
	private static long hours;
	private static long days;
	private static long weeks;
	private static long months;
	private static long years;
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	public static Logger logger  = Logger.getLogger(Duration.class);

	public Duration(String startDate) {
		calculateDuration(startDate);
	}

	public static String calculateDuration(String startDate) {
		Date currentDate = new Date();
		Date d1 = null;
		Date d2 = null;

		try {
			d1 = dateFormat.parse(startDate);
			d2 = dateFormat.parse(dateFormat.format(currentDate));

			// in milliseconds
			long diff = d2.getTime() - d1.getTime();

			seconds = diff / 1000 % 60;
			minutes = diff / (60 * 1000) % 60;
			hours = diff / (60 * 60 * 1000) % 24;
			days = diff / (24 * 60 * 60 * 1000);

			if (days >= 7) {
				years = days / 365;

				long remainingDays = days % 365;

				months = remainingDays / 30;

				remainingDays = remainingDays % 30;

				weeks = remainingDays / 7;

				days = remainingDays % 7;

			}

		} catch (Exception e) {
			logger.error("calculate duration error",e);
		}

		if (years > 0) {
			return years + (years > 1 ? " years" : " year") + " ago.";
		} else if (months > 0) {
			return months + (months > 1 ? " months" : " month") + " ago.";
		} else if (weeks > 0) {
			return weeks + (weeks > 1 ? " weeks" : " week") + " ago.";
		} else if (days > 0) {
			return days + (days > 1 ? " days" : " day") + " ago.";
		} else if (hours > 0) {
			return hours + (hours > 1 ? " hours" : " hour") + " ago.";
		} else if (minutes > 0) {
			return minutes + (minutes > 1 ? " minutes" : " minute") + " ago.";
		} else {
			return seconds + (seconds > 1 ? " seconds" : " second") + " ago.";
		}

	}

	public static String calculateDuration(long startDate) {
		Date date = new Date(startDate);
		return calculateDuration(dateFormat.format(date));
	}

}
