package com.biphy.model;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.log4j.Logger;

import com.biphy.util.ResourceUtil;
import com.biphy.workplace.BiphyServices;
import com.biphy.workplace.Workplace;

import javafx.geometry.Insets;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;

public class BiphyTrayHeader extends TrayItems {

	private StackPane newRecordPane;

	private Image companyIcon;

	private ImageView companyIconView;

	private Hyperlink myChannel;

	private Stage stage;

	private Logger logger = Logger.getLogger(BiphyTrayHeader.class);

	private StackPane companyIconViewPane;

	private StackPane closeButton;

	public BiphyTrayHeader(Stage stage) {
		super();
		this.stage = stage;
		this.type = TrayItemType.BifyHeader;
		this.trayItem = this;
		companyIcon = new Image(ResourceUtil.getImage("biphy-logo-100x36.png").toExternalForm());
		createNewRecordLogo();
		createCompanyLogo();
		createChannelLink();
		createCloseButton();
	}

	public Image getCompanyIcon() {
		return companyIcon;
	}

	public void setCompanyIcon(Image companyIcon) {
		this.companyIcon = companyIcon;
	}

	public ImageView getCompanyIconView() {
		return companyIconView;
	}

	public void setCompanyIconView(ImageView companyIconView) {
		this.companyIconView = companyIconView;
	}

	public Hyperlink getMyChannel() {
		return myChannel;
	}

	public void setMyChannel(Hyperlink myChannel) {
		this.myChannel = myChannel;
	}

	public StackPane getNewRecordPane() {
		return newRecordPane;
	}

	public void setNewRecordPane(StackPane newRecordPane) {
		this.newRecordPane = newRecordPane;
	}

	public StackPane getCloseButton() {
		return closeButton;
	}

	private void createCompanyLogo() {
		companyIconViewPane = new StackPane();
		companyIconViewPane.setPadding(new Insets(0,50,0,0));
		companyIconView = new ImageView(companyIcon);
		companyIconView.setFitWidth(80);
		companyIconView.setFitHeight(35);
		companyIconViewPane.getChildren().add(companyIconView);
	}

	private void createChannelLink() {
		myChannel = new Hyperlink("channel");
		myChannel.setTextFill(Color.web("#7140aa"));
		myChannel.setStyle("-fx-font-weight: bold  ;  ");//-fx-padding : 5px 5px 0px 5px ;
		myChannel.setOnMousePressed(m -> {
			try {
				Desktop.getDesktop().browse(new URL("http://www.biphy.co/account").toURI());
			} catch (IOException | URISyntaxException e) {
				// TODO Auto-generated catch block
				logger.error("createChannelLink ioexception / urlsyntaxexception", e);
			}

		});

//		myChannel.setPadding(new Insets(0, 0, 0, 20));
	}

	private void createNewRecordLogo() {
		newRecordPane = new StackPane();
		Rectangle newRecordRect = new Rectangle(0, 0, 32, 22);
//		Circle circle = new Circle(0, 0, 1);

		newRecordPane.setBackground(null);
		newRecordRect.setFill(null);
		newRecordRect.setStrokeWidth(3);
		newRecordRect.setStroke(Color.rgb(113, 64, 170));
		newRecordRect.setStrokeWidth(1);
		newRecordRect.getStrokeDashArray().addAll(3.0, 7.0, 3.0, 7.0);
		Label newRecordLabel = new Label("new");
		newRecordLabel.setTextFill(Color.web("#7140aa"));
		newRecordLabel.setStyle("-fx-font-weight: bold");
		newRecordLabel.setOnMousePressed(m -> {
			Workplace.getStage().setScene(null);
			Workplace.getStage().close();
			if (Workplace.getCustomSystemTrayIcons() != null) {
				Workplace.getCustomSystemTrayIcons().removeRecordButton();
			}

			Workplace.getService(BiphyServices.Show_Record).launch();

		});
		newRecordPane.getChildren().add(newRecordRect);
		newRecordPane.getChildren().add(newRecordLabel);
		newRecordPane.setPadding(new Insets(0, 0, 0, 0));
	}


	private void createCloseButton(){
		closeButton = new StackPane();
		Label close = new Label("x");

		closeButton.setOnMousePressed(m->{
			Workplace.getCustomSystemTrayPopup().removePopup();
		});
		close.setTooltip(new Tooltip("Hide Popup"));
		closeButton.setBackground(null);
		close.setTextFill(Color.web("#7140aa"));
		close.setStyle("-fx-font-weight: bold");
		closeButton.getChildren().add(close);
//		closeButton.setPadding(new Insets(0, 0, 33, 25));
	}

}
