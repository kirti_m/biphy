package com.biphy.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BiphyRecord implements Comparable<BiphyRecord> {
	@JsonProperty(value = "file_id", index = 0)
	private String fileId;

	@JsonProperty(value = "file_name", index = 1)
	private String fileName;
	@JsonProperty(value = "file_size", index = 2)
	private String fileSize;
	@JsonProperty(value = "local_file_path", index = 3)
	private String localFilePath;
	@JsonProperty(value = "remote_file_path", index = 4)
	private String remoteFilePath;
	@JsonProperty(value = "thumbnail_path", index = 5)
	private String thumbNail;
	@JsonProperty(value = "created_on", index = 6)
	private long created_on;

	public BiphyRecord(String fileId, String fileName, String fileSize, String localFilePath, String remoteFilePath,
			String thumbNail, long created_on) {
		super();
		this.fileId = fileId;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.localFilePath = localFilePath;
		this.remoteFilePath = remoteFilePath;
		this.thumbNail = thumbNail;
		this.created_on = created_on;
	}

	public BiphyRecord() {
		super();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public long getCreated_on() {
		return created_on;
	}

	public void setCreated_on(long created_on) {
		this.created_on = created_on;
	}

	public String getLocalFilePath() {
		return localFilePath;
	}

	public void setLocalFilePath(String localFilePath) {
		this.localFilePath = localFilePath;
	}

	public String getRemoteFilePath() {
		return remoteFilePath;
	}

	public void setRemoteFilePath(String remoteFilePath) {
		this.remoteFilePath = remoteFilePath;
	}

	public String getThumbNail() {
		return thumbNail;
	}

	public void setThumbNail(String thumbNail) {
		this.thumbNail = thumbNail;
	}

	@Override
	public String toString() {
		return "BiphyRecord [fileId=" + fileId + ", fileName=" + fileName + ", fileSize=" + fileSize
				+ ", localFilePath=" + localFilePath + ", remoteFilePath=" + remoteFilePath + ", created_on="
				+ created_on + "]";
	}

	public int compareTo(BiphyRecord biph) {
		return Long.compare(biph.getCreated_on(), getCreated_on());

	}
}
