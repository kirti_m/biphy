
package com.biphy.model;


import com.biphy.util.ResourceUtil;
import com.biphy.workplace.Workplace;
import javafx.scene.Node;
import javafx.geometry.NodeOrientation;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.effect.*;
import javafx.scene.paint.*;


public class BiphyTrayRecord extends TrayItems {

	private String bifName;

	private Image thumbnailImage;

	private ImageView thumImageView;
	
	private Text text;

	private String uploadedTime;

	private Image deleteIcon = new Image(ResourceUtil.getImage("delete-50x50.png").toString());

	private StackPane deleteIconView;

	private ProgressBar progressBar;

	private Hyperlink remoteUrl;

	private BiphyRecord record;

	public BiphyTrayRecord(String bifName, Image thumbnailImage, String uploadedTime, BiphyRecord record) {
		super();
		this.bifName = bifName;
		this.thumbnailImage = thumbnailImage;
		this.uploadedTime = uploadedTime;
		this.thumImageView = new ImageView(thumbnailImage);
		this.thumImageView.setFitWidth(50);
		this.thumImageView.setFitHeight(40);
		this.thumImageView.setEffect(new DropShadow (BlurType.GAUSSIAN,Color.PURPLE,1, 2, 0, 0));
		this.thumImageView.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
		this.trayItem = this;
		this.record = record;
		this.type = TrayItemType.BifyRecords;
	}

	public String getBifName() {
		return bifName;
	}

	public void setBifName(String bifName) {
		this.bifName = bifName;
	}

	public Image getThumbnailImage() {
		return thumbnailImage;
	}

	public void setThumbnailImage(Image thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}

	public ImageView getThumImageView() {
		return thumImageView;
	}

	public void setThumImageView(ImageView thumImageView) {
		this.thumImageView = thumImageView;
	}

	public String getUploadedTime() {
		return uploadedTime;
	}

	public void setUploadedTime(String uploadedTime) {
		this.uploadedTime = uploadedTime;
	}

	public Image getDeleteIcon() {
		return deleteIcon;
	}

	public void setDeleteIcon(Image deleteIcon) {
		this.deleteIcon = deleteIcon;
	}

	public StackPane getDeleteIconView() {
		if (deleteIconView != null) {
			return deleteIconView;
		} else {
			deleteIconView = new StackPane();
			ImageView deleteIconImageView = new ImageView(this.deleteIcon);
			deleteIconImageView.setFitHeight(30);
			deleteIconImageView.setFitWidth(30);
			deleteIconView.getChildren().add(deleteIconImageView);
			deleteIconView.setOnMousePressed(m -> {
				Workplace.getWorkplace().getBiphyRecords().removeBiphyRecord(record);
			});
			return deleteIconView;
		}
	}

	public ProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(ProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	public Hyperlink getRemoteUrl() {
		return remoteUrl;
	}

	public void setRemoteUrl(Hyperlink remoteUrl) {
		this.remoteUrl = remoteUrl;
	}

	public BiphyRecord getRecord() {
		return record;
	}

	public void setRecord(BiphyRecord record) {
		this.record = record;
	}

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

	public BiphyTrayRecord(Text text) {
		super();
		this.text = text;
		this.type = TrayItemType.BifyRecords;

	}

	


}
