package com.biphy.main;

import javafx.stage.Stage;

public abstract class RecorderScene {

	protected Stage stage;

	public RecorderScene(Stage stage) {
		super();
		this.stage = stage;
	}

	public abstract void launch();

	public abstract void nextScene(Stage stage);

	public abstract boolean isLaunched();



}
