package com.biphy.main;

import com.biphy.custom.widgets.CustomTrayPopup;
import com.biphy.util.ResourceUtil;
import com.biphy.util.os.MacOsUtil;
import com.biphy.workplace.BiphyServices;
import com.biphy.workplace.Workplace;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class BiphyRecorder extends Application {
	@Override
	public void start(Stage primaryStage) {
		Platform.setImplicitExit(false);
		primaryStage.initStyle(StageStyle.TRANSPARENT);
		primaryStage.getIcons()
				.add(new Image(ResourceUtil.getImage("biphy-tray-icon-purple-32x32.png").toExternalForm()));
		Workplace.setStage(primaryStage);
		Workplace.initialiseService(primaryStage, true);
		if (!Workplace.checkIfApplicationIsRunning()) {
			Workplace.getService(BiphyServices.Splash_Screen).launch();
		}
		Workplace.setCustomSystemTrayPopup(Workplace.getCustomSystemTrayPopup() == null ? new CustomTrayPopup(null, primaryStage)
				: Workplace.getCustomSystemTrayPopup());
		Workplace.getCustomSystemTrayPopup().initializePanel();
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		if (Workplace.getOsUtil() != null && Workplace.getOsUtil() instanceof MacOsUtil) {
			Workplace.getOsUtil().destroyMacRectangle();
		}
	}

	public static void main(String[] args) {
		// Assign o to output stream

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				if (Workplace.getOsUtil() != null && Workplace.getOsUtil() instanceof MacOsUtil) {
					Workplace.getOsUtil().destroyMacRectangle();
					Platform.exit();
				}
			}
		}));
		launch(args);

	}

}
