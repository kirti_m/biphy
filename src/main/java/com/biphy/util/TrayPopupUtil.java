package com.biphy.util;

import java.awt.Desktop;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.log4j.Logger;

import com.biphy.custom.widgets.AlertDialog;
import com.biphy.model.BiphyRecord;
import com.biphy.model.BiphyTrayHeader;
import com.biphy.model.BiphyTrayRecord;
import com.biphy.model.Duration;
import com.biphy.model.TrayItems;
import com.biphy.workplace.OperatingSystem;
import com.biphy.workplace.Workplace;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.*;

public class TrayPopupUtil {

	private static Logger logger = Logger.getLogger(TrayPopupUtil.class);

	public static Dimension getPreferredSize(Rectangle2D screenSize) {
		Dimension dimension = new Dimension();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		return dimension;
	}

	public static VBox createBiphyTrayItems(BiphyTrayRecord biphyTrayRecord) {
		VBox mainBox = new VBox(1);
		HBox box = new HBox(1);
		box.getChildren().add(biphyTrayRecord.getThumImageView());
		VBox vBox = new VBox(5);
		Label name = new Label(biphyTrayRecord.getBifName()+".gif");
		name.setFont(Font.font ("Verdana", FontWeight.BOLD,11));
		Label updated = new Label(biphyTrayRecord.getUploadedTime());
		vBox.getChildren().add(name);
		vBox.getChildren().add(updated);
		vBox.setOnMouseClicked(m->{
			openFileInImageViewer(biphyTrayRecord.getRecord());
		});
		HBox recordUtilBar = new HBox(1);
		vBox.getChildren().add(recordUtilBar);
		vBox.setPadding(new Insets(7,10,5,10));
		box.getChildren().add(vBox);
		box.getChildren().add(biphyTrayRecord.getDeleteIconView());
		box.setUserData(biphyTrayRecord);

		Separator seprator =new Separator(Orientation.HORIZONTAL);
	    seprator.getStyleClass().add("seprator");
	    seprator.setValignment(VPos.BOTTOM);
	    seprator.setMinSize(1,1);
		enableDragging(box, biphyTrayRecord.getRecord());
		box.setAlignment(Pos.CENTER);
		mainBox.getChildren().add(box);
		mainBox.getChildren().add(seprator);
		return mainBox;
	}

	private static void enableDragging(HBox box, BiphyRecord record) {

		box.setOnDragDetected(m -> {
			try {
				Clipboard clipboard = Clipboard.getSystemClipboard();
				Dragboard db = box.startDragAndDrop(TransferMode.ANY);
				ClipboardContent content = new ClipboardContent();
				File gifFile = new File(record.getLocalFilePath());
				File gifFileCpy = File.createTempFile(record.getFileId(), ".gif");
				Path figFileCpyPath = gifFileCpy.toPath();
				gifFileCpy.delete();
				if (gifFile.exists()) {
					Files.copy(gifFile.toPath(), figFileCpyPath);
					content.putFiles(java.util.Collections.singletonList(gifFileCpy));
					db.setContent(content);
					clipboard.setContent(content);
					m.consume();
				}
			} catch (IOException e) {
				logger.error("error occuured while drag drop cpy ", e);
			}

		});

	}

	public static HBox createBiphyTrayHeader(BiphyTrayHeader biphyTrayHeader) {
		HBox box = new HBox(15);
		if (Workplace.isWindows()) {
			box.setId("header-win");
		
		} else if (Workplace.isMacOs()) {
			box.setId("header-mac");
		}
//		box.setStyle("-fx-background-color:#E3E5EB;");
		box.getChildren().add(biphyTrayHeader.getNewRecordPane());
		box.setMargin(biphyTrayHeader.getNewRecordPane(), new Insets(0,-10,0, 0));

		box.getChildren().add(biphyTrayHeader.getCompanyIconView());
		box.setMargin(biphyTrayHeader.getCompanyIconView(), new Insets(0, 0,0, 30));
		box.getChildren().add(biphyTrayHeader.getMyChannel());
		box.setMargin(biphyTrayHeader.getMyChannel(), new Insets(0,0,0,25));

//		box.getChildren().add(biphyTrayHeader.getCloseButton());
//		box.setMargin(biphyTrayHeader.getCloseButton(), new Insets(-30, 0,0, 10));

		box.setPadding(new Insets(10, 10, 10, 10));
		box.setUserData(biphyTrayHeader);
		box.setAlignment(Pos.CENTER);
		return box;
	}

	public static Point2D calculateRectangle(Point2D mouseEvent, double width, double height, OperatingSystem os) {
		if (os.equals(OperatingSystem.Window)) {
			double x = mouseEvent.getX() - (width / 2);
			double y = mouseEvent.getY() - height + 5;
			return new Point2D(x, y);
		} else if (os.equals(OperatingSystem.MACOS)) {
			double x = mouseEvent.getX() - (width / 2);
			double y = (mouseEvent.getY() > 10 ? mouseEvent.getY() + 5 : mouseEvent.getY() + 15);
			return new Point2D(x, y);
		}
		return null;
	}

	public static TrayItems getTrayItemFromBiphyRecord(BiphyRecord record) throws MalformedURLException {
		if(record == null || record.getThumbNail() == null){
			return null;
		}


		BiphyTrayRecord trayItem = new BiphyTrayRecord(record.getFileId(),
				new Image(new File(record.getThumbNail()).toURI().toURL().toExternalForm()),
				Duration.calculateDuration(record.getCreated_on()), record);
		return trayItem;
	}

	public static void openFileInImageViewer(BiphyRecord record) {
		String imageUrl = record.getRemoteFilePath();
		if (imageUrl != null) {
			try {
				Desktop.getDesktop().browse(new URI(imageUrl));
			} catch (IOException | URISyntaxException e) {
				// TODO Auto-generated catch block
				logger.error("open image in desktop ioexception", e);
			}
		} else {
			AlertDialog alertDialog = new AlertDialog(Workplace.getStage(), "Url not found",
					"Url was not found or invalid url");
			alertDialog.showWidget(false);
		}

	}

}
