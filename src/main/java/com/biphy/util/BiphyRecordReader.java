package com.biphy.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.biphy.model.BiphyRecord;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BiphyRecordReader {

	private File userData;

	private Logger logger = Logger.getLogger(BiphyRecordReader.class);

	public BiphyRecordReader(File userData) {
		this.userData = userData;
	}

	public List<BiphyRecord> readBiphyRecord() {
		try {
			String fileContent = new String(Files.readAllBytes(userData.toPath()));
			if (fileContent.length() > 1) {
				ObjectMapper objectMapper = new ObjectMapper();
				BiphyRecord[] pojos = objectMapper.readValue(fileContent, BiphyRecord[].class);
				List<BiphyRecord> recordList = new ArrayList<BiphyRecord>(Arrays.asList(pojos));
				return recordList;
			}
			return null;
		} catch (JsonParseException e) {
			logger.error("readBiphyRecord json parse exception",e);
		} catch (JsonMappingException e) {
			logger.error("readBiphyRecord json mapping exception",e);
		} catch (IOException e) {
			logger.error("readBiphyRecord ioexception",e);
		}
		return null;
	}
}
