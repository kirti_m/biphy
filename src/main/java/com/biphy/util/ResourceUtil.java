package com.biphy.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Properties;

public class ResourceUtil {

	private static String setupPath;

	public static void setSetupPath(String path) {
		setupPath = path;
	}

	public static URL getResource(String location) {
		return ClassLoader.getSystemClassLoader().getResource(location);
	}

	public static URL getCss(String css) {
		return getResource("main/resources/css".concat("/").concat(css));
	}

	public static URL getImage(String image) {
		return getResource("main/resources/img".concat("/").concat(image));
	}

	public static URL getFXML(String fxml) {
		return getResource("main/resources/fxml".concat("/").concat(fxml));
	}

	public static URL getConfig(String config) {
		return getResource("main/resources/config".concat("/").concat(config));
	}

	public static URL getExternal(String external) {
		return getResource("main/resources/ext".concat("/").concat(external));
	}

	public static URL getCertificate(String certificate) {
		return getResource("main/resources/cert".concat("/").concat(certificate));
	}

	public static URL getExternalProd(String external) throws MalformedURLException {
		return new File(setupPath + "/" + external).toURI().toURL();
	}

	public static URL getCertProd(String cert) throws MalformedURLException {
		return new File(setupPath + "/" + cert).toURI().toURL();
	}

	public static File getMacConfigFile() {
		File macosConfigProperties = new File("Contents/Java/lib".concat("/").concat("macos-config.properties"));
		System.out.println("mac os config : " + macosConfigProperties.getAbsolutePath());
		if (macosConfigProperties.exists()) {
			return macosConfigProperties;
		}
		return null;
	}

	public static URI getMacOsFiles(String path) throws MalformedURLException {
		return new File("Contents/Java/lib".concat(path)).toURI();
	}

	public static void addToPropertyFile(String key , String value) {
		File macosConfigProperties = new File("Contents/Java/lib".concat("/").concat("macos-config.properties"));
		try {
			if(macosConfigProperties.exists()) {
	        	InputStream propertyInputStream = new FileInputStream(macosConfigProperties);
	        	Properties properties = new Properties();
	        	properties.load(propertyInputStream);
	        	properties.setProperty(key, value);
	        	propertyInputStream.close();
	        	OutputStream propertyOutputStream = new FileOutputStream(macosConfigProperties);
	        	properties.store(propertyOutputStream,"content modified");
	        	propertyOutputStream.flush();
	        	propertyOutputStream.close();
	        }	
		}catch(Exception e) {
			System.out.println("Exception occured while storing :"+key+" with value : "+value);
			e.printStackTrace();
		}
        
	}

}
