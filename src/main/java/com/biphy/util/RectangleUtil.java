package com.biphy.util;

import java.util.ArrayList;
import java.util.List;

import com.biphy.custom.widgets.CustomRectangle;
import com.biphy.custom.widgets.CustomRectangle.RectPoints;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeType;
import javafx.stage.Screen;

public class RectangleUtil {

	private static Rectangle2D screenSize = Screen.getPrimary().getBounds();
	private static Point2D centerOfRect;

	public static Rectangle getRect(Point2D mouseClicked, Point2D mouseReleased) {
		// Get Center of Screen
		centerOfRect = getMidCoordinates(mouseClicked, mouseReleased);
		RectPoints mouseClickedQuadrant = indetifyQuadrant(mouseClicked);
		RectPoints mouseReleasedQuadrant = indetifyQuadrant(mouseReleased);

		Rectangle rectangle = null;
		if (mouseClickedQuadrant != null && mouseReleasedQuadrant != null) {

			switch (mouseClickedQuadrant) {
			case A:
				if (mouseReleasedQuadrant == RectPoints.C) {
					WidthHeight widhtHeight = calculateDimension(mouseClicked, mouseReleased);
					rectangle = new Rectangle(mouseClicked.getX(), mouseClicked.getY(), widhtHeight.getWidth(),
							widhtHeight.getHeight());
				}
				break;
			case B:
				if (mouseReleasedQuadrant == RectPoints.D) {
					WidthHeight widhtHeight = calculateDimension(mouseClicked, mouseReleased);

					rectangle = new Rectangle(mouseReleased.getX(), mouseClicked.getY(), widhtHeight.getWidth(),
							widhtHeight.getHeight());
				}
				break;
			case C:
				if (mouseReleasedQuadrant == RectPoints.A) {
					WidthHeight widhtHeight = calculateDimension(mouseClicked, mouseReleased);
					rectangle = new Rectangle(mouseReleased.getX(), mouseReleased.getY(), widhtHeight.getWidth(),
							widhtHeight.getHeight());
				}
				break;
			case D:
				if (mouseReleasedQuadrant == RectPoints.B) {
					WidthHeight widhtHeight = calculateDimension(mouseClicked, mouseReleased);
					rectangle = new Rectangle(mouseClicked.getX(), mouseReleased.getY(), widhtHeight.getWidth(),
							widhtHeight.getHeight());
				}
				break;

			default:
				break;
			}
		}

		if (rectangle != null) {
			rectangle.setFill(Color.rgb(0, 0, 0, 0.4));
			return rectangle;
		}
		return null;

	}

	private static WidthHeight calculateDimension(Point2D mouseClicked, Point2D mouseReleased) {
		WidthHeight widthHeight = new WidthHeight();
		widthHeight.setWidth(Math.abs(mouseClicked.getX() - mouseReleased.getX()));
		widthHeight.setHeight(Math.abs(mouseClicked.getY() - mouseReleased.getY()));
		return widthHeight;
	}

	private static RectPoints indetifyQuadrant(Point2D point) {

		double x = centerOfRect.getX() - point.getX();
		double y = centerOfRect.getY() - point.getY();

		if (x > 0 && y > 0) {
			// quadrant 1
			return RectPoints.A;
		} else if (x < 0 && y > 0) {
			// quadrant 2
			return RectPoints.B;

		} else if (x < 0 && y < 0) {
			// quadrant 3
			return RectPoints.C;

		} else if (y < 0 && x > 0) {
			// quadrant 4
			return RectPoints.D;

		}
		return null;

	}

	public static Point2D getMidCoordinates(Point2D mouseClicked, Point2D mouseReleased) {
		if (mouseClicked != null && mouseReleased != null) {
			return new Point2D(((mouseClicked.getX() + mouseReleased.getX()) / 2),
					((mouseClicked.getY() + mouseReleased.getY()) / 2));
		}
		return null;
	}

	public static void makeRectangleTransperent(Rectangle rectangle) {
		rectangle.setStrokeType(StrokeType.OUTSIDE);
		rectangle.setStroke(Color.web("#7140aa"));
		rectangle.setStrokeDashOffset(20);
		rectangle.setStrokeWidth(2);
		rectangle.getStrokeDashArray().addAll(3.0, 7.0, 3.0, 7.0);
		rectangle.setFill(Color.rgb(255,255,255,0.06));
		rectangle.setStrokeLineCap(StrokeLineCap.SQUARE);
	}

	public static Rectangle resizeRectangleFromEdges(Rectangle rectangle, RectPoints rectPoints,
			Point2D newCoordinate) {

		Rectangle newRectangle = null;
		switch (rectPoints) {
		case A:
			if (rectPoints.C.getCoordinates() != null) {
				newRectangle = getRect(newCoordinate, rectPoints.C.getCoordinates());
			}

			break;
		case B:
			if (rectPoints.D.getCoordinates() != null) {
				newRectangle = getRect(newCoordinate, rectPoints.D.getCoordinates());
			}
			break;
		case C:
			if (rectPoints.A.getCoordinates() != null) {
				newRectangle = getRect(newCoordinate, rectPoints.A.getCoordinates());
			}
			break;
		case D:
			if (rectPoints.B.getCoordinates() != null) {
				newRectangle = getRect(newCoordinate, rectPoints.B.getCoordinates());
			}

			break;

		default:
			break;
		}
		if (newRectangle != null) {
			newRectangle.setFill(Color.rgb(0, 0, 0, 0.6));
			return newRectangle;
		}
		return null;
	}


	public static List<RectPoints> createNewRectangleEdges(Rectangle rectangle){
		List<RectPoints> points = new ArrayList<CustomRectangle.RectPoints>();
		RectPoints.A.setCoordinates(new Point2D(rectangle.getX(), rectangle.getY()));
		double width = rectangle.getWidth();
		double height = rectangle.getHeight();
		RectPoints.C.setCoordinates(new Point2D(rectangle.getX() + width, rectangle.getY() + height));
		RectPoints.B.setCoordinates(
				new Point2D(RectPoints.A.getCoordinates().getX(), RectPoints.C.getCoordinates().getY()));
		RectPoints.D.setCoordinates(
				new Point2D(RectPoints.C.getCoordinates().getX(), RectPoints.A.getCoordinates().getY()));

		points.add(RectPoints.A);
		points.add(RectPoints.C);
		points.add(RectPoints.B);
		points.add(RectPoints.D);
		return points;
	}

	private static class WidthHeight {

		private double width;

		private double height;

		public double getWidth() {
			return width;
		}

		public void setWidth(double width) {
			this.width = width;
		}

		public double getHeight() {
			return height;
		}

		public void setHeight(double height) {
			this.height = height;
		}

		@Override
		public String toString() {
			return "WidthHeight [width=" + width + ", height=" + height + "]";
		}

	}

}
