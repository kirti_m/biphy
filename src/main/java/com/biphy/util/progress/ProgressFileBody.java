package com.biphy.util.progress;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.output.CountingOutputStream;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.content.FileBody;

public class ProgressFileBody extends FileBody {

	public ProgressFileBody(File file, ContentType contentType, String filename) {
		super(file, contentType, filename);
		// TODO Auto-generated constructor stub
		fileSize = file.length();
	}

	private ProgressBarListener listener;

    private long fileSize;

    private long completed;


	@Override
	public void writeTo(OutputStream out) throws IOException {
		CountingOutputStream output = new CountingOutputStream(out) {
			@Override
			protected void beforeWrite(int n) {

				if (listener != null && n != 0)
					listener.counterChanged(calculatePercentage(n));
				super.beforeWrite(n);
			}
		};
		super.writeTo(output);

	}

	public void setListener(ProgressBarListener listener) {
		this.listener = listener;
	}

	public ProgressBarListener getListener() {
		return listener;
	}

	private double calculatePercentage(int n){
		completed = completed + n;
		return ((double)completed/fileSize)*100;
	}

}
