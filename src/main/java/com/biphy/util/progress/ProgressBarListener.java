package com.biphy.util.progress;

public interface ProgressBarListener {
	void counterChanged(double delta);
}
