package com.biphy.util;

import java.io.File;
import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.biphy.model.BiphyRecord;
import com.biphy.model.Duration;

public class FileUtil {

	private static Logger logger = Logger.getLogger(FileUtil.class);

	public static String getFileSize(String filePath) {
		File fileSz = new File(filePath);
		float fileSize = (fileSz.length() / 1024) / 1024;
		return Math.abs(fileSize) + " MB";
	}

	public static long getCreatedOn() {
		Date currentDate = new Date();
		try {
			return Duration.dateFormat.parse(Duration.dateFormat.format(currentDate)).getTime();
		} catch (ParseException e) {
			logger.error("getCreatedOn parse exception ", e);
		}
		return 0;
	}

	public static void deleteBiphyRecord(BiphyRecord record) {
		File gifFile = new File(record.getLocalFilePath());
		File thumbnailFile = new File(record.getThumbNail());
		if (gifFile.exists()) {
			gifFile.delete();
		}
		if (thumbnailFile.exists()) {
			if(!thumbnailFile.delete()){
				thumbnailFile.deleteOnExit();
			}
		}
	}

}
