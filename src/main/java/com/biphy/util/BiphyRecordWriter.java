package com.biphy.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.biphy.model.BiphyRecord;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BiphyRecordWriter {

	private File userData;

	private Logger logger = Logger.getLogger(BiphyRecordWriter.class);

	public BiphyRecordWriter(File userData) {
		super();
		this.userData = userData;
	}

	public void writeBiphyRecord(List<BiphyRecord> biphyRecordList) {

		if (biphyRecordList == null) {
			biphyRecordList = new ArrayList<BiphyRecord>();
		}

		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writeValue(userData, biphyRecordList);
		} catch (IOException e) {
			logger.error("writeBiphyRecord logger", e);
		}
	}
}