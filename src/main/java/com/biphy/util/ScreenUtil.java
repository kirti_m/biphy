package com.biphy.util;

import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.shape.Rectangle;
import javafx.stage.Screen;

public class ScreenUtil {

	private List<java.awt.Rectangle> bounds;

	private int screenCounts;

	private Dimension dimension;

	public ScreenUtil() {
		super();
		bounds = new ArrayList<java.awt.Rectangle>();
		screenCounts = 0;
		initializeScreens();
	}

	private void initializeScreens() {
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		int width = 0;
		int height = 0;
		int maxWidth = 0;
		int maxHeight = 0;
		for (int i = 0; i < gs.length; i++) {
			java.awt.Rectangle rect = gs[i].getDefaultConfiguration().getBounds();
			bounds.add(gs[i].getDefaultConfiguration().getBounds());
			width += rect.width;
			if (width > maxWidth) {
				maxWidth = width;
			}
			height += rect.height;
			if (rect.getHeight() > maxHeight) {
				maxHeight = rect.height;
			}
			screenCounts++;
		}
		width += maxWidth;
		height += maxHeight;
		dimension = new Dimension(width, height);
	}

	public int getScreenCounts() {
		return screenCounts;
	}

	public Dimension getDimension() {
		return dimension;
	}

	public boolean isMultiScreenSetup() {
		if (screenCounts > 1) {
			return true;
		} else {
			return false;
		}
	}

}
