package com.biphy.util.os;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.log4j.Logger;

import com.biphy.component.BiphyRecords;
import com.biphy.model.BiphyRecord;
import com.biphy.util.FileUtil;
import com.biphy.util.GifsicleBuilder;
import com.biphy.util.ResourceUtil;
import com.biphy.workplace.Workplace;

public class WindowOsUtil implements OsUtil {

	private String osArcitecture;

	private final String win32gifsicleName = "gifsicle-win-32.exe";
	private final String win64gifsicleName = "gifsicle-win-64.exe";

	private final Logger logger = Logger.getLogger(WindowOsUtil.class);

	public WindowOsUtil() {
		String arch = System.getenv("PROCESSOR_ARCHITECTURE");
		String wow64Arch = System.getenv("PROCESSOR_ARCHITEW6432");

		osArcitecture = arch != null && arch.endsWith("64") || wow64Arch != null && wow64Arch.endsWith("64") ? "64"
				: "32";

	}

	@Override
	public boolean checkIfApplicationIsRunning() {
		return false;
	}

	@Override
	public BiphyRecord compressGif(BiphyRecords recordUtil, File gifFile, BiphyRecord record) {

		String gifRecordName = record.getFileId();

		try {
			File tempFile = File.createTempFile(gifRecordName, ".gif");
			String tempFilePath = tempFile.getAbsolutePath();
			tempFile.delete();

			String processName = null;
			if (osArcitecture.equals("64")) {
				processName = win64gifsicleName;
			} else if (osArcitecture.equals(32)) {
				processName = win64gifsicleName;
			}
			if (processName == null) {
				return null;
			}

			String gifsicleArgs = GifsicleBuilder.getBuilder(processName).setLevel("O3").withConserveMemory()
					.isVerbose().setScale(0.8).setDelay(14).isCareful().withSourceFile(record.getLocalFilePath())
					.withDestinationFile(tempFilePath).noWarning().build();

			File processPath = null;
			if (Workplace.isProduction) {
				processPath = new File(ResourceUtil.getExternalProd("ext/"+processName).getFile());
			} else {
				processPath = new File(ResourceUtil.getExternal(processName).getFile());
			}
			String processAbsoulutePath = processPath.getAbsolutePath().replaceAll("%20", " ");
			logger.info("Gifscicle String :  "+processAbsoulutePath+" "+gifsicleArgs);
			Process gifscileProcess = Runtime.getRuntime().exec(processAbsoulutePath + " " + gifsicleArgs);

			createInputStream(true,gifscileProcess,processAbsoulutePath);
			createInputStream(false,gifscileProcess,processAbsoulutePath);
			gifscileProcess.waitFor();

			File tempFileProcessed = new File(tempFilePath);
			File sourceFile = new File(record.getLocalFilePath());

			if (tempFileProcessed.length() < sourceFile.length()) {
				Path sourceFilePath = sourceFile.toPath();
				sourceFile.delete();
				Path finalFilePath = Files.copy(tempFileProcessed.toPath(), sourceFilePath);
				record.setLocalFilePath(finalFilePath.toAbsolutePath().toString());
				record.setFileSize(FileUtil.getFileSize(finalFilePath.toAbsolutePath().toString()));
				return record;
			} else {
				tempFileProcessed.delete();
				tempFileProcessed.deleteOnExit();
				return record;
			}
		} catch (IOException e) {
			logger.error("IOException Error occured while compressing gif : " + record.getFileId(), e);
		} catch (InterruptedException e) {
			logger.error("InterruptedException occured while compressing gif : " + record.getFileId(), e);

		}

		return null;
	}


	private void createInputStream(boolean isInput, Process process, String processPath) {
		InputStreamReader reader = null;
		try {
			if (isInput) {
				reader = new InputStreamReader(process.getInputStream());
			} else {
				reader = new InputStreamReader(process.getErrorStream());
			}

			BufferedReader bufferedReader = new BufferedReader(reader);
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				System.out.println(processPath + (isInput ? " Input " : " Error") + " --> " + line);
				logger.info(processPath + (isInput ? " Input " : " Error") + " --> " + line);
			}
		} catch (IOException e) {
			logger.error("Error occured while creating input stream : ", e);
		}
	}

	@Override
	public void createMacRectangle(double rectx, double recty, double width, double height) {
       // only implemented in macos util
	}
	@Override
	public void destroyMacRectangle() {
	       // only implemented in macos util
	}

}
