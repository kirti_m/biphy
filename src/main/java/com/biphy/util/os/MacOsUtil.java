package com.biphy.util.os;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.lf5.util.ResourceUtils;

import com.biphy.component.BiphyRecords;
import com.biphy.custom.widgets.AlertDialog;
import com.biphy.model.BiphyRecord;
import com.biphy.util.FileUtil;
import com.biphy.util.GifsicleBuilder;
import com.biphy.util.ResourceUtil;
import com.biphy.workplace.Workplace;

public class MacOsUtil implements OsUtil {

	private static Process macP;

	private Map<String, File> librariesPath = new HashMap<>();

	private Logger logger = Logger.getLogger(MacOsUtil.class);

	private final String macgifsicleName = "gifsicle";

	public static final String appName = "Biphy.app";

	public static File appPath;

	public MacOsUtil() {
		System.setProperty("apple.awt.UIElement", "true");
	}

	public MacOsUtil(Map<String, File> librariesPath) {
		super();
		this.librariesPath = librariesPath;
		setPermissionForApplication();
	}

	@Override
	public boolean checkIfApplicationIsRunning() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BiphyRecord compressGif(BiphyRecords recordUtil, File gifFile, BiphyRecord record) {
		String gifRecordName = record.getFileId();

		try {
			File tempFile = File.createTempFile(gifRecordName, ".gif");
			String tempFilePath = tempFile.getAbsolutePath();
			tempFile.delete();

			String gifsicleArgs = GifsicleBuilder.getBuilder(macgifsicleName).setLevel("O3").withConserveMemory()
					.isVerbose().setScale(0.8).setDelay(14).isCareful().withSourceFile(record.getLocalFilePath())
					.withDestinationFile(tempFilePath).noWarning().build();

			if (!librariesPath.containsKey("com.biphy.gifsicle")) {
				return null;
			}

			Process gifscileProcess = Runtime.getRuntime()
					.exec(librariesPath.get("com.biphy.lib") + " " + gifsicleArgs);
			createInputStream(true, gifscileProcess, "Gifscile Process");
			createInputStream(false, gifscileProcess, "Gifscile Process");

			gifscileProcess.waitFor();
			File tempFileProcessed = new File(tempFilePath);
			File sourceFile = new File(record.getLocalFilePath());

			if (tempFileProcessed.length() < sourceFile.length()) {
				Path sourceFilePath = sourceFile.toPath();
				sourceFile.delete();
				Path finalFilePath = Files.copy(tempFileProcessed.toPath(), sourceFilePath);
				record.setLocalFilePath(finalFilePath.toAbsolutePath().toString());
				record.setFileSize(FileUtil.getFileSize(finalFilePath.toAbsolutePath().toString()));
				return record;
			} else {
				if (!tempFileProcessed.delete()) {
					tempFileProcessed.deleteOnExit();
				}
				return record;
			}
		} catch (InterruptedException | IOException e) {
			System.out.println("Error occured while compressing file : " + e.getMessage());
			e.printStackTrace();
			logger.error("Error occured while compressing gif : ", e);
		}

		return null;

	}

	@Override
	public void createMacRectangle(double rectx, double recty, double width, double height) {
		try {
			logger.error("libraries path : " + librariesPath);
			if (librariesPath.containsKey("com.biphy.jdk6.java") && librariesPath.containsKey("com.biphy.runnable")) {

				System.out.println(librariesPath.get("com.biphy.jdk6.java").getAbsolutePath() + " -jar "
						+ librariesPath.get("com.biphy.runnable").getAbsolutePath() + " " + (int) rectx + " " + (int) recty
						+ " " + (int) width + " " + (int) height);

				macP = Runtime.getRuntime()
						.exec(librariesPath.get("com.biphy.jdk6.java").getAbsolutePath() + " -Dapple.awt.UIElement=true -jar "
								+ librariesPath.get("com.biphy.runnable").getAbsolutePath() + " " + (int) rectx + " "
								+ (int) recty + " " + (int) width + " " + (int) height);

				createInputStream(true, macP, "Mac Runnable");
				createInputStream(false, macP, "Mac Runnable");

			}

		} catch (IOException e) {
			System.out.println("Exception occured while building process : " + e.getMessage());
			e.printStackTrace();
			logger.error("Exception occured while running mac runnable jar : ", e);
		}

	}

	private void createInputStream(boolean isInput, Process process, String processPath) {
		InputStreamReader reader = null;
		try {
			if (isInput) {
				reader = new InputStreamReader(process.getInputStream());
			} else {
				reader = new InputStreamReader(process.getErrorStream());
			}

			BufferedReader bufferedReader = new BufferedReader(reader);
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				System.out.println(processPath + (isInput ? " Input " : "Error") + " --> " + line);
				logger.error(processPath + (isInput ? " Input " : "Error") + " --> " + line);
			}
		} catch (IOException e) {
			logger.error("Error occured while creating input stream : ", e);
		}
	}

	@Override
	public void destroyMacRectangle() {
		if (macP != null && macP.isAlive()) {
			macP.destroy();
		}
	}

	private void setPermissionForApplication() {
		Set<PosixFilePermission> perms = new HashSet<>();
		perms.add(PosixFilePermission.OWNER_READ);
		perms.add(PosixFilePermission.OWNER_WRITE);
		perms.add(PosixFilePermission.OWNER_EXECUTE);

		perms.add(PosixFilePermission.OTHERS_READ);
		perms.add(PosixFilePermission.OTHERS_WRITE);
		perms.add(PosixFilePermission.OTHERS_EXECUTE);

		perms.add(PosixFilePermission.GROUP_READ);
		perms.add(PosixFilePermission.GROUP_WRITE);
		perms.add(PosixFilePermission.GROUP_EXECUTE);

		File jdkFile = librariesPath.get("com.biphy.jdk6.java");
		File jarFile = librariesPath.get("com.biphy.mac.access.jar");
		File resourceRulesFiles = librariesPath.get("com.biphy.mac.accessibility");
		File applicationFile = (librariesPath.containsKey("com.biphy.app.path")?librariesPath.get("com.biphy.app.path"):getAppFile(appName));
		File libFile = librariesPath.get("com.biphy.lib");
		File bashScriptFile = librariesPath.get("com.biphy.mac.access.sh");
        System.out.println("Libraries : "+librariesPath);
		System.out.println("Jdk File : " + jdkFile.getAbsolutePath());
		System.out.println("Jdk File exe : " + jdkFile.canExecute());

		System.out.println("jar File : " + jarFile.getAbsolutePath());
		System.out.println("jarFile File exe : " + jarFile.canExecute());

		System.out.println("libFile File : " + libFile.getAbsolutePath());
		System.out.println("libFile File exe : " + libFile.canExecute());

		System.out.println("resourceRulesFiles File : " + resourceRulesFiles.getAbsolutePath());
		System.out.println("resourceRulesFiles File : " + resourceRulesFiles.canExecute());

		System.out.println("applicationFile File : " + applicationFile.getAbsolutePath());
		System.out.println("applicationFile exec : " + applicationFile.canExecute());

		System.out.println("bashScriptFile File : " + bashScriptFile.getAbsolutePath());
		System.out.println("bashScriptFile exec : " + bashScriptFile.canExecute());

		try {
			Set<PosixFilePermission> presentPermission = Files.getPosixFilePermissions(resourceRulesFiles.toPath(),
					LinkOption.NOFOLLOW_LINKS);
			perms.removeAll(presentPermission);
			if (perms.size() > 0) {

				logger.info("Need to set permission");

				File tempFile = File.createTempFile("temp", ".properties");
				if (tempFile.exists() && jdkFile.exists() && resourceRulesFiles.exists() && applicationFile.exists()
						&& jarFile.exists() && libFile.exists() && bashScriptFile.exists()) {
					Properties properties = new Properties();
					properties.setProperty("jdk_1.6_path", jdkFile.getAbsolutePath());
					properties.setProperty("resource_rules", resourceRulesFiles.getAbsolutePath());
					properties.setProperty("application", applicationFile.getAbsolutePath());
					properties.setProperty("lib",libFile.getAbsolutePath());
					OutputStream propertiesOutputStream = new FileOutputStream(tempFile);
					properties.store(propertiesOutputStream, "temp file");

					System.out.println("Command : " + bashScriptFile.getAbsolutePath() + " " + System.getProperty("java.home")
							+ "/bin/java " + " " + jarFile.getAbsolutePath() + " " + tempFile.getAbsolutePath());

					Process applicationPrivilege = Runtime.getRuntime()
							.exec("sh " + bashScriptFile.getAbsolutePath() + " " + System.getProperty("java.home")
									+ "/bin/java " + " " + jarFile.getAbsolutePath() + " "
									+ tempFile.getAbsolutePath());
					
					
					createInputStream(true, applicationPrivilege, "Provide Accessibility Previlege : ");
					createInputStream(false, applicationPrivilege, "Provide Accessibility Previlege : ");
				     if(applicationPrivilege.waitFor() != 0) {
                         AlertDialog alertDialog = new AlertDialog(Workplace.getStage(),"Permission Required","To make changes and record gif , we require some permission for creating folders");
                         alertDialog.showWidgetAndCloseApplication(700,120);
				     }

					

				}
			}

		} catch (IOException | InterruptedException e1) {
			logger.error("error occured while setting permission : ", e1);
			e1.printStackTrace();
		}

	}

	public static File getAppFile(String appName) {
		if (appPath == null) {
			File file = new File(".");
			String projectPath = file.getAbsolutePath().substring(0, file.getAbsolutePath().length() - 1);
			File projectPathFile = new File(projectPath);

			if (projectPathFile.isDirectory()) {
				File getFile = searchFile(projectPathFile, appName);
				if (getFile != null) {
					appPath = getFile;
					return getFile;
				}
			}

		}
		return appPath;
	}

	private static File searchFile(File directoryPath, String appName) {

		for (File file : directoryPath.listFiles()) {
			if (file.getName().equalsIgnoreCase(appName)) {
				return file;
			}

		}

		return searchFile(new File(FilenameUtils.getFullPathNoEndSeparator(directoryPath.getAbsolutePath())), appName);
	}

	public File getMacResource(String resource) {
		if (librariesPath.containsKey(resource)) {
			return librariesPath.get(resource);
		} else {
			return null;
		}
	}

}
