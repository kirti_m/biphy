package com.biphy.util.os;

import java.io.File;

import com.biphy.component.BiphyRecords;
import com.biphy.model.BiphyRecord;

public interface OsUtil {


	public boolean checkIfApplicationIsRunning();

	public BiphyRecord compressGif(BiphyRecords recordUtil , File gifFile , BiphyRecord record);

	public void createMacRectangle(double rectx,double recty,double width,double height);

	public void destroyMacRectangle();
}
