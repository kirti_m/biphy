package com.biphy.util;

public class GifsicleBuilder {

	private static StringBuilder argsBuilder;

	private static GifsicleBuilder builder;

	private static GifScale gifScale;

	public static GifsicleBuilder getBuilder(String appName) {
		argsBuilder = new StringBuilder();
		builder = new GifsicleBuilder();
		gifScale = builder.new GifScale();
		return builder;
	}

	public GifsicleBuilder setLevel(String level) {
		gifScale.setLevel(level);
		return builder;
	}

	public GifsicleBuilder withConserveMemory() {
		gifScale.setConserveMemory(true);
		return builder;
	}

	public GifsicleBuilder isVerbose() {
		gifScale.setVerbose(true);
		return builder;
	}

	public GifsicleBuilder setScale(double scale) {
		// argsBuilder.append(" --scale " + scale + " ");
		gifScale.setScale(scale);
		return builder;
	}

	public GifsicleBuilder setDelay(int delay) {
		// argsBuilder.append(" --d " + scale + " ");
		gifScale.setDelay(delay);
		return builder;
	}

	public GifsicleBuilder isCareful() {
		// argsBuilder.append(" --careful ");
		gifScale.setCareful(true);
		return builder;
	}

	public GifsicleBuilder useColor(String color) {
		// argsBuilder.append(" --use-col=" + color + " ");
		gifScale.setUseColor(color);
		return builder;
	}

	public GifsicleBuilder setColors(int colors) {
		gifScale.setColors(colors);
		return builder;
	}

	public GifsicleBuilder withSourceFile(String sourceFilePath) {
		gifScale.setSourceFilePath(sourceFilePath);
		return builder;
	}

	public GifsicleBuilder withDestinationFile(String destinationFilePath) {
		gifScale.setDestinationFilePath(destinationFilePath);
		return builder;
	}

	public GifsicleBuilder noWarning() {
		gifScale.setNoWarning(true);
		return builder;
	}

	public String build() {
		if (gifScale.getSourceFilePath() != null && gifScale.getDestinationFilePath() != null) {
			argsBuilder.append((gifScale.getLevel() != null ? "-" + gifScale.getLevel() + " " : "-03 "))
					.append((gifScale.isConserveMemory() ? " --conserve-memory " : ""))
					.append((gifScale.isVerbose() ? " --verbose " : ""))
					.append(((gifScale.getColors() > 2 && gifScale.getColors() < 256)
							? " --colors=" + gifScale.getColors() + " " : ""))
					.append((gifScale.getUseColor() != null ? " --use-col=" + gifScale.getUseColor() + " " : ""))
					.append((gifScale.getScale() > 0 ? " --scale " + gifScale.getScale() + " " : ""))
					.append((gifScale.getDelay() > 0 ? " -d " + gifScale.getDelay() + " " : ""))
					.append((gifScale.isCareful() ? " --careful " : ""))
					.append((gifScale.isNoWarning() ? " --no-warnings " : ""))
					.append((gifScale.getSourceFilePath() != null ? " " + gifScale.getSourceFilePath() + " " : ""))
					.append(" -o ").append((gifScale.getDestinationFilePath() != null
							? " " + gifScale.getDestinationFilePath() + " " : ""));
			return argsBuilder.toString();
		}
		return null;
	}

	private class GifScale {

		private String level;

		private int colors;

		private String useColor;

		private double scale;

		private boolean isCareful;

		private boolean isConserveMemory;

		private boolean isVerbose;

		private int delay;

		private String sourceFilePath;

		private String destinationFilePath;

		private boolean noWarning;

		public String getLevel() {
			return level;
		}

		public void setLevel(String level) {
			this.level = level;
		}

		public int getColors() {
			return colors;
		}

		public void setColors(int colors) {
			this.colors = colors;
		}

		public String getUseColor() {
			return useColor;
		}

		public void setUseColor(String useColor) {
			this.useColor = useColor;
		}

		public double getScale() {
			return scale;
		}

		public void setScale(double scale) {
			this.scale = scale;
		}

		public boolean isCareful() {
			return isCareful;
		}

		public void setCareful(boolean isCareful) {
			this.isCareful = isCareful;
		}

		public boolean isConserveMemory() {
			return isConserveMemory;
		}

		public void setConserveMemory(boolean isConserveMemory) {
			this.isConserveMemory = isConserveMemory;
		}

		public boolean isVerbose() {
			return isVerbose;
		}

		public void setVerbose(boolean isVerbose) {
			this.isVerbose = isVerbose;
		}

		public int getDelay() {
			return delay;
		}

		public void setDelay(int delay) {
			this.delay = delay;
		}

		public String getSourceFilePath() {
			return sourceFilePath;
		}

		public void setSourceFilePath(String sourceFilePath) {
			this.sourceFilePath = sourceFilePath;
		}

		public String getDestinationFilePath() {
			return destinationFilePath;
		}

		public void setDestinationFilePath(String destinationFilePath) {
			this.destinationFilePath = destinationFilePath;
		}

		public boolean isNoWarning() {
			return noWarning;
		}

		public void setNoWarning(boolean noWarning) {
			this.noWarning = noWarning;
		}
	}

}
