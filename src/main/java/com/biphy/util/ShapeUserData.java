package com.biphy.util;

public class ShapeUserData {

	private String key;

	private String value;

	private Object obj;

	public ShapeUserData(String key, String value, Object obj) {
		super();
		this.key = key;
		this.value = value;
		this.obj = obj;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	public Object getObj() {
		return obj;
	}


}
